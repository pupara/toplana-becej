<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PocetnaController@show')->name('home');

Route::get('/istorijat-i-organizacija', 'IstorijatController@show')->name('istorijat');

Route::get('/tehnicki-podaci', 'TehnickiPodaciController@show')->name('tehnicki-podaci');

Route::get('/organizacija', 'OrganizacijaController@show')->name('organizacija');

Route::get('/korisnicki-servis', 'KorisnickiServisController@show')->name('korisnicki-servis');

Route::get('/kontakt', 'KontaktController@show')->name('kontakt');

Route::get('/javne-nabavke', 'JavneNabavkeController@show')->name('javne-nabavke');

Route::get('/odluke', 'JavneNabavkeController@showArhiva')->name('arhiva');

Route::get('/vesti', 'VestiController@showVesti')->name('vesti');
Route::get('/vesti/{slug}', 'VestiController@VestiSlug')->name('vestiSlug');
Route::get('/aktuelnosti', 'VestiController@showAktuelnosti')->name('aktuelnosti');
Route::get('/aktuelnosti/{slug}', 'VestiController@aktuelnostiSlug')->name('aktuelnostiSlug');


Route::get('/finansijski-izvestaji', 'JavneNabavkeController@showIzvestaj')->name('finansijski-izvestaji');




Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('nabavkas', 'nabavkaController');

Route::resource('vests', 'vestController');

Route::resource('aktuelnosts', 'aktuelnostController');

Route::resource('cenas', 'cenaController');

Route::resource('brojilos', 'brojiloController');

Route::resource('grejnas', 'grejnaController');