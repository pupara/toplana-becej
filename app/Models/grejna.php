<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class grejna
 * @package App\Models
 * @version February 21, 2017, 9:33 pm UTC
 */
class grejna extends Model
{
    use SoftDeletes;

    public $table = 'grejnas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'tekst'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'tekst' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
