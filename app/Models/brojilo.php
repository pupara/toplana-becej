<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class brojilo
 * @package App\Models
 * @version February 21, 2017, 8:50 pm UTC
 */
class brojilo extends Model
{
    use SoftDeletes;

    public $table = 'brojilos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'naslov',
        'tekst',
        'slika'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'naslov' => 'string',
        'tekst' => 'string',
        'slika' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
