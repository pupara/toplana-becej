<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cena
 * @package App\Models
 * @version February 21, 2017, 8:48 pm UTC
 */
class cena extends Model
{
    use SoftDeletes;

    public $table = 'cenas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'naslov',
        'tekst'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'naslov' => 'string',
        'tekst' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
