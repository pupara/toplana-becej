<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class vest
 * @package App\Models
 * @version February 17, 2017, 7:51 pm UTC
 */
class vest extends Model
{
    use SoftDeletes;

    public $table = 'vests';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'naslov',
        'tekst',
        'tip',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'naslov' => 'string',
        'tekst' => 'string',
        'tip' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
