<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class aktuelnost
 * @package App\Models
 * @version February 17, 2017, 9:29 pm UTC
 */
class aktuelnost extends Model
{
    use SoftDeletes;

    public $table = 'aktuelnosts';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'naslov',
        'tekst',
        'slug'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'naslov' => 'string',
        'tekst' => 'string',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
