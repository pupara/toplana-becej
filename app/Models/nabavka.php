<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class nabavka
 * @package App\Models
 * @version February 17, 2017, 10:47 am UTC
 */
class nabavka extends Model
{
    use SoftDeletes;

    public $table = 'nabavkas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'naziv',
        'dokument',
        'arhiva'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'naziv' => 'string',
        'dokument' => 'string',
        'arhiva' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'naziv' => 'required'
    ];

    
}
