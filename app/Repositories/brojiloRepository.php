<?php

namespace App\Repositories;

use App\Models\brojilo;
use InfyOm\Generator\Common\BaseRepository;

class brojiloRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'naslov',
        'tekst',
        'slika'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return brojilo::class;
    }
}
