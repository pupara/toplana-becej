<?php

namespace App\Repositories;

use App\Models\nabavka;
use InfyOm\Generator\Common\BaseRepository;
use Illuminate\Support\Facades\DB;

class nabavkaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'naziv',
        'dokument',
        'arhiva'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return nabavka::class;
    }

    public function nabavke()
    {
    return nabavka::where('arhiva','nabavka');
    }

    public function arhiva()
    {
        return nabavka::where('arhiva','odluka');
    }

    public function izvestaj()
    {
        return nabavka::where('arhiva','izvestaj');
    }
}