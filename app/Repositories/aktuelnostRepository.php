<?php

namespace App\Repositories;

use App\Models\aktuelnost;
use InfyOm\Generator\Common\BaseRepository;

class aktuelnostRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'naslov',
        'tekst',
        'slug'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return aktuelnost::class;
    }
}
