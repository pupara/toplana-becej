<?php

namespace App\Repositories;

use App\Models\vest;
use InfyOm\Generator\Common\BaseRepository;

class vestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'naslov',
        'tekst',
        'slug'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return vest::class;
    }

    public function vest()
    {
        return vest::where('tip','vest');
    }

    public function aktuelnost()
    {
        return vest::where('tip','aktuelnost');
    }

    public function top()
    {
        return vest::where('tip','vest')->limit(5)->get();
    }

}
