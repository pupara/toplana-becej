<?php

namespace App\Repositories;

use App\Models\grejna;
use InfyOm\Generator\Common\BaseRepository;

class grejnaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tekst'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return grejna::class;
    }
}
