<?php

namespace App\Repositories;

use App\Models\cena;
use InfyOm\Generator\Common\BaseRepository;

class cenaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'naslov',
        'tekst'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cena::class;
    }
}
