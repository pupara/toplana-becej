<?php

namespace App\Http\Traits;

trait WeatherTrait
{

    public function getWeather()
    {
        $url = "http://api.openweathermap.org/data/2.5/weather?id=792814&lang=en&units=metric&APPID=34c007b0579d9501be841bf84ab2641d";
        $contents = file_get_contents($url);
        $clima = json_decode($contents);

        return $clima;
    }

}
