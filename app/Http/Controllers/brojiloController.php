<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatebrojiloRequest;
use App\Http\Requests\UpdatebrojiloRequest;
use App\Repositories\brojiloRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class brojiloController extends AppBaseController
{
    /** @var  brojiloRepository */
    private $brojiloRepository;

    public function __construct(brojiloRepository $brojiloRepo)
    {
        $this->brojiloRepository = $brojiloRepo;
    }

    /**
     * Display a listing of the brojilo.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->brojiloRepository->pushCriteria(new RequestCriteria($request));
        $brojilos = $this->brojiloRepository->all();

        return view('brojilos.index')
            ->with('brojilos', $brojilos);
    }

    /**
     * Show the form for creating a new brojilo.
     *
     * @return Response
     */
    public function create()
    {
        return view('brojilos.create');
    }

    /**
     * Store a newly created brojilo in storage.
     *
     * @param CreatebrojiloRequest $request
     *
     * @return Response
     */
    public function store(CreatebrojiloRequest $request)
    {
        $input = $request->all();

        if(!empty($request->file('slika'))){
            $request->file('slika')->move(public_path('img/cust'), $request->file('slika')->getClientOriginalName());
            $imageName ='img\cust\\'. $request->file('slika')->getClientOriginalName();

            $input = array_except($input, ['slika']);
            $input = array_add($input, 'slika', $imageName);
        }

        $brojilo = $this->brojiloRepository->create($input);

        Flash::success('Brojilo saved successfully.');

        return redirect(route('brojilos.index'));
    }

    /**
     * Display the specified brojilo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $brojilo = $this->brojiloRepository->findWithoutFail($id);

        if (empty($brojilo)) {
            Flash::error('Brojilo not found');

            return redirect(route('brojilos.index'));
        }

        return view('brojilos.show')->with('brojilo', $brojilo);
    }

    /**
     * Show the form for editing the specified brojilo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $brojilo = $this->brojiloRepository->findWithoutFail($id);

        if (empty($brojilo)) {
            Flash::error('Brojilo not found');

            return redirect(route('brojilos.index'));
        }

        return view('brojilos.edit')->with('brojilo', $brojilo);
    }

    /**
     * Update the specified brojilo in storage.
     *
     * @param  int              $id
     * @param UpdatebrojiloRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatebrojiloRequest $request)
    {
        $brojilo = $this->brojiloRepository->findWithoutFail($id);

        $input = $request->all();

        if (empty($brojilo)) {
            Flash::error('Brojilo not found');

            return redirect(route('brojilos.index'));
        }
        if(!empty($request->file('slika'))){
            $request->file('slika')->move(public_path('img/cust'), $request->file('slika')->getClientOriginalName());
            $imageName ='img\cust\\'. $request->file('slika')->getClientOriginalName();

            $input = array_except($input, ['slika']);
            $input = array_add($input, 'slika', $imageName);
        }
        $brojilo = $this->brojiloRepository->update($input, $id);

        Flash::success('Brojilo updated successfully.');

        return redirect(route('brojilos.index'));
    }

    /**
     * Remove the specified brojilo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $brojilo = $this->brojiloRepository->findWithoutFail($id);

        if (empty($brojilo)) {
            Flash::error('Brojilo not found');

            return redirect(route('brojilos.index'));
        }

        $this->brojiloRepository->delete($id);

        Flash::success('Brojilo deleted successfully.');

        return redirect(route('brojilos.index'));
    }
}
