<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\WeatherTrait;
use App\Models\vest;
use App\Models\aktuelnost;



class PocetnaController extends Controller
{
    use WeatherTrait;


    public function show(Request $request)
    {
        $blogs= vest::where('tip','vest')->limit(5)->get();
        $aktuelnost= vest::where('tip','aktuelnost')->orderBy('id', 'DESC')->first();
        $clima=$this->getWeather();

        $data = array(
            'blogs'  => $blogs,
            'aktuelnost' => $aktuelnost,
            'clima'   => $clima);

        return view('index')->with($data);
    }



}
