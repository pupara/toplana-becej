<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateaktuelnostRequest;
use App\Http\Requests\UpdateaktuelnostRequest;
use App\Repositories\aktuelnostRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class aktuelnostController extends AppBaseController
{
    /** @var  aktuelnostRepository */
    private $aktuelnostRepository;

    public function __construct(aktuelnostRepository $aktuelnostRepo)
    {
        $this->aktuelnostRepository = $aktuelnostRepo;
    }

    /**
     * Display a listing of the aktuelnost.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->aktuelnostRepository->pushCriteria(new RequestCriteria($request));
        $aktuelnosts = $this->aktuelnostRepository->all();

        return view('aktuelnosts.index')
            ->with('aktuelnosts', $aktuelnosts);
    }

    /**
     * Show the form for creating a new aktuelnost.
     *
     * @return Response
     */
    public function create()
    {
        return view('aktuelnosts.create');
    }



    public function store(CreateaktuelnostRequest $request)
    {
        $input = $request->all();

        $slug = str_slug($request->naslov);
        $input = array_add($input, 'slug', $slug);


        $aktuelnost = $this->aktuelnostRepository->create($input);

        Flash::success('Aktuelnost saved successfully.');

        return redirect(route('aktuelnosts.index'));
    }




    public function show($id)
    {
        $aktuelnost = $this->aktuelnostRepository->findWithoutFail($id);

        if (empty($aktuelnost)) {
            Flash::error('Aktuelnost not found');

            return redirect(route('aktuelnosts.index'));
        }

        return view('aktuelnosts.show')->with('aktuelnost', $aktuelnost);
    }

    /**
     * Show the form for editing the specified aktuelnost.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $aktuelnost = $this->aktuelnostRepository->findWithoutFail($id);

        if (empty($aktuelnost)) {
            Flash::error('Aktuelnost not found');

            return redirect(route('aktuelnosts.index'));
        }

        return view('aktuelnosts.edit')->with('aktuelnost', $aktuelnost);
    }

    /**
     * Update the specified aktuelnost in storage.
     *
     * @param  int              $id
     * @param UpdateaktuelnostRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateaktuelnostRequest $request)
    {
        $aktuelnost = $this->aktuelnostRepository->findWithoutFail($id);

        if (empty($aktuelnost)) {
            Flash::error('Aktuelnost not found');

            return redirect(route('aktuelnosts.index'));
        }

        $aktuelnost = $this->aktuelnostRepository->update($request->all(), $id);

        Flash::success('Aktuelnost updated successfully.');

        return redirect(route('aktuelnosts.index'));
    }

    /**
     * Remove the specified aktuelnost from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $aktuelnost = $this->aktuelnostRepository->findWithoutFail($id);

        if (empty($aktuelnost)) {
            Flash::error('Aktuelnost not found');

            return redirect(route('aktuelnosts.index'));
        }

        $this->aktuelnostRepository->delete($id);

        Flash::success('Aktuelnost deleted successfully.');

        return redirect(route('aktuelnosts.index'));
    }
}
