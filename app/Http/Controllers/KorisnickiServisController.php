<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\WeatherTrait;
use App\Repositories\cenaRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\cena;
use App\Models\brojilo;
use App\Models\grejna;




class KorisnickiServisController extends Controller
{
    use WeatherTrait;

    public function show(Request $request)
    {

        $clima=$this->getWeather();

        $cena= cena::first();
        $brojilo= brojilo::first();
        $grejna= grejna::first();

        $data = array(
            'cena' => $cena,
            'brojilo' => $brojilo,
            'grejna' => $grejna,
            'clima'   => $clima);


        return view('korisnicki-servis')->with($data);
    }

}
