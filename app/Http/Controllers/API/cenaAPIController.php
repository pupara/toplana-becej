<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatecenaAPIRequest;
use App\Http\Requests\API\UpdatecenaAPIRequest;
use App\Models\cena;
use App\Repositories\cenaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class cenaController
 * @package App\Http\Controllers\API
 */

class cenaAPIController extends AppBaseController
{
    /** @var  cenaRepository */
    private $cenaRepository;

    public function __construct(cenaRepository $cenaRepo)
    {
        $this->cenaRepository = $cenaRepo;
    }

    /**
     * Display a listing of the cena.
     * GET|HEAD /cenas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->cenaRepository->pushCriteria(new RequestCriteria($request));
        $this->cenaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $cenas = $this->cenaRepository->all();

        return $this->sendResponse($cenas->toArray(), 'Cenas retrieved successfully');
    }

    /**
     * Store a newly created cena in storage.
     * POST /cenas
     *
     * @param CreatecenaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatecenaAPIRequest $request)
    {
        $input = $request->all();

        $cenas = $this->cenaRepository->create($input);

        return $this->sendResponse($cenas->toArray(), 'Cena saved successfully');
    }

    /**
     * Display the specified cena.
     * GET|HEAD /cenas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var cena $cena */
        $cena = $this->cenaRepository->findWithoutFail($id);

        if (empty($cena)) {
            return $this->sendError('Cena not found');
        }

        return $this->sendResponse($cena->toArray(), 'Cena retrieved successfully');
    }

    /**
     * Update the specified cena in storage.
     * PUT/PATCH /cenas/{id}
     *
     * @param  int $id
     * @param UpdatecenaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecenaAPIRequest $request)
    {
        $input = $request->all();

        /** @var cena $cena */
        $cena = $this->cenaRepository->findWithoutFail($id);

        if (empty($cena)) {
            return $this->sendError('Cena not found');
        }

        $cena = $this->cenaRepository->update($input, $id);

        return $this->sendResponse($cena->toArray(), 'cena updated successfully');
    }

    /**
     * Remove the specified cena from storage.
     * DELETE /cenas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var cena $cena */
        $cena = $this->cenaRepository->findWithoutFail($id);

        if (empty($cena)) {
            return $this->sendError('Cena not found');
        }

        $cena->delete();

        return $this->sendResponse($id, 'Cena deleted successfully');
    }
}
