<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreategrejnaAPIRequest;
use App\Http\Requests\API\UpdategrejnaAPIRequest;
use App\Models\grejna;
use App\Repositories\grejnaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class grejnaController
 * @package App\Http\Controllers\API
 */

class grejnaAPIController extends AppBaseController
{
    /** @var  grejnaRepository */
    private $grejnaRepository;

    public function __construct(grejnaRepository $grejnaRepo)
    {
        $this->grejnaRepository = $grejnaRepo;
    }

    /**
     * Display a listing of the grejna.
     * GET|HEAD /grejnas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->grejnaRepository->pushCriteria(new RequestCriteria($request));
        $this->grejnaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $grejnas = $this->grejnaRepository->all();

        return $this->sendResponse($grejnas->toArray(), 'Grejnas retrieved successfully');
    }

    /**
     * Store a newly created grejna in storage.
     * POST /grejnas
     *
     * @param CreategrejnaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreategrejnaAPIRequest $request)
    {
        $input = $request->all();

        $grejnas = $this->grejnaRepository->create($input);

        return $this->sendResponse($grejnas->toArray(), 'Grejna saved successfully');
    }

    /**
     * Display the specified grejna.
     * GET|HEAD /grejnas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var grejna $grejna */
        $grejna = $this->grejnaRepository->findWithoutFail($id);

        if (empty($grejna)) {
            return $this->sendError('Grejna not found');
        }

        return $this->sendResponse($grejna->toArray(), 'Grejna retrieved successfully');
    }

    /**
     * Update the specified grejna in storage.
     * PUT/PATCH /grejnas/{id}
     *
     * @param  int $id
     * @param UpdategrejnaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdategrejnaAPIRequest $request)
    {
        $input = $request->all();

        /** @var grejna $grejna */
        $grejna = $this->grejnaRepository->findWithoutFail($id);

        if (empty($grejna)) {
            return $this->sendError('Grejna not found');
        }

        $grejna = $this->grejnaRepository->update($input, $id);

        return $this->sendResponse($grejna->toArray(), 'grejna updated successfully');
    }

    /**
     * Remove the specified grejna from storage.
     * DELETE /grejnas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var grejna $grejna */
        $grejna = $this->grejnaRepository->findWithoutFail($id);

        if (empty($grejna)) {
            return $this->sendError('Grejna not found');
        }

        $grejna->delete();

        return $this->sendResponse($id, 'Grejna deleted successfully');
    }
}
