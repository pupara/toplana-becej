<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatenabavkaAPIRequest;
use App\Http\Requests\API\UpdatenabavkaAPIRequest;
use App\Models\nabavka;
use App\Repositories\nabavkaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class nabavkaController
 * @package App\Http\Controllers\API
 */

class nabavkaAPIController extends AppBaseController
{
    /** @var  nabavkaRepository */
    private $nabavkaRepository;

    public function __construct(nabavkaRepository $nabavkaRepo)
    {
        $this->nabavkaRepository = $nabavkaRepo;
    }

    /**
     * Display a listing of the nabavka.
     * GET|HEAD /nabavkas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->nabavkaRepository->pushCriteria(new RequestCriteria($request));
        $this->nabavkaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $nabavkas = $this->nabavkaRepository->all();

        return $this->sendResponse($nabavkas->toArray(), 'Nabavkas retrieved successfully');
    }

    /**
     * Store a newly created nabavka in storage.
     * POST /nabavkas
     *
     * @param CreatenabavkaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatenabavkaAPIRequest $request)
    {
        $input = $request->all();

        $nabavkas = $this->nabavkaRepository->create($input);

        return $this->sendResponse($nabavkas->toArray(), 'Nabavka saved successfully');
    }

    /**
     * Display the specified nabavka.
     * GET|HEAD /nabavkas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var nabavka $nabavka */
        $nabavka = $this->nabavkaRepository->findWithoutFail($id);

        if (empty($nabavka)) {
            return $this->sendError('Nabavka not found');
        }

        return $this->sendResponse($nabavka->toArray(), 'Nabavka retrieved successfully');
    }

    /**
     * Update the specified nabavka in storage.
     * PUT/PATCH /nabavkas/{id}
     *
     * @param  int $id
     * @param UpdatenabavkaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatenabavkaAPIRequest $request)
    {
        $input = $request->all();

        /** @var nabavka $nabavka */
        $nabavka = $this->nabavkaRepository->findWithoutFail($id);

        if (empty($nabavka)) {
            return $this->sendError('Nabavka not found');
        }

        $nabavka = $this->nabavkaRepository->update($input, $id);

        return $this->sendResponse($nabavka->toArray(), 'nabavka updated successfully');
    }

    /**
     * Remove the specified nabavka from storage.
     * DELETE /nabavkas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var nabavka $nabavka */
        $nabavka = $this->nabavkaRepository->findWithoutFail($id);

        if (empty($nabavka)) {
            return $this->sendError('Nabavka not found');
        }

        $nabavka->delete();

        return $this->sendResponse($id, 'Nabavka deleted successfully');
    }
}
