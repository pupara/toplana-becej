<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatebrojiloAPIRequest;
use App\Http\Requests\API\UpdatebrojiloAPIRequest;
use App\Models\brojilo;
use App\Repositories\brojiloRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class brojiloController
 * @package App\Http\Controllers\API
 */

class brojiloAPIController extends AppBaseController
{
    /** @var  brojiloRepository */
    private $brojiloRepository;

    public function __construct(brojiloRepository $brojiloRepo)
    {
        $this->brojiloRepository = $brojiloRepo;
    }

    /**
     * Display a listing of the brojilo.
     * GET|HEAD /brojilos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->brojiloRepository->pushCriteria(new RequestCriteria($request));
        $this->brojiloRepository->pushCriteria(new LimitOffsetCriteria($request));
        $brojilos = $this->brojiloRepository->all();

        return $this->sendResponse($brojilos->toArray(), 'Brojilos retrieved successfully');
    }

    /**
     * Store a newly created brojilo in storage.
     * POST /brojilos
     *
     * @param CreatebrojiloAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatebrojiloAPIRequest $request)
    {
        $input = $request->all();

        $brojilos = $this->brojiloRepository->create($input);

        return $this->sendResponse($brojilos->toArray(), 'Brojilo saved successfully');
    }

    /**
     * Display the specified brojilo.
     * GET|HEAD /brojilos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var brojilo $brojilo */
        $brojilo = $this->brojiloRepository->findWithoutFail($id);

        if (empty($brojilo)) {
            return $this->sendError('Brojilo not found');
        }

        return $this->sendResponse($brojilo->toArray(), 'Brojilo retrieved successfully');
    }

    /**
     * Update the specified brojilo in storage.
     * PUT/PATCH /brojilos/{id}
     *
     * @param  int $id
     * @param UpdatebrojiloAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatebrojiloAPIRequest $request)
    {
        $input = $request->all();

        /** @var brojilo $brojilo */
        $brojilo = $this->brojiloRepository->findWithoutFail($id);

        if (empty($brojilo)) {
            return $this->sendError('Brojilo not found');
        }

        $brojilo = $this->brojiloRepository->update($input, $id);

        return $this->sendResponse($brojilo->toArray(), 'brojilo updated successfully');
    }

    /**
     * Remove the specified brojilo from storage.
     * DELETE /brojilos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var brojilo $brojilo */
        $brojilo = $this->brojiloRepository->findWithoutFail($id);

        if (empty($brojilo)) {
            return $this->sendError('Brojilo not found');
        }

        $brojilo->delete();

        return $this->sendResponse($id, 'Brojilo deleted successfully');
    }
}
