<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateaktuelnostAPIRequest;
use App\Http\Requests\API\UpdateaktuelnostAPIRequest;
use App\Models\aktuelnost;
use App\Repositories\aktuelnostRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class aktuelnostController
 * @package App\Http\Controllers\API
 */

class aktuelnostAPIController extends AppBaseController
{
    /** @var  aktuelnostRepository */
    private $aktuelnostRepository;

    public function __construct(aktuelnostRepository $aktuelnostRepo)
    {
        $this->aktuelnostRepository = $aktuelnostRepo;
    }

    /**
     * Display a listing of the aktuelnost.
     * GET|HEAD /aktuelnosts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->aktuelnostRepository->pushCriteria(new RequestCriteria($request));
        $this->aktuelnostRepository->pushCriteria(new LimitOffsetCriteria($request));
        $aktuelnosts = $this->aktuelnostRepository->all();

        return $this->sendResponse($aktuelnosts->toArray(), 'Aktuelnosts retrieved successfully');
    }

    /**
     * Store a newly created aktuelnost in storage.
     * POST /aktuelnosts
     *
     * @param CreateaktuelnostAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateaktuelnostAPIRequest $request)
    {
        $input = $request->all();

        $aktuelnosts = $this->aktuelnostRepository->create($input);

        return $this->sendResponse($aktuelnosts->toArray(), 'Aktuelnost saved successfully');
    }

    /**
     * Display the specified aktuelnost.
     * GET|HEAD /aktuelnosts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var aktuelnost $aktuelnost */
        $aktuelnost = $this->aktuelnostRepository->findWithoutFail($id);

        if (empty($aktuelnost)) {
            return $this->sendError('Aktuelnost not found');
        }

        return $this->sendResponse($aktuelnost->toArray(), 'Aktuelnost retrieved successfully');
    }

    /**
     * Update the specified aktuelnost in storage.
     * PUT/PATCH /aktuelnosts/{id}
     *
     * @param  int $id
     * @param UpdateaktuelnostAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateaktuelnostAPIRequest $request)
    {
        $input = $request->all();

        /** @var aktuelnost $aktuelnost */
        $aktuelnost = $this->aktuelnostRepository->findWithoutFail($id);

        if (empty($aktuelnost)) {
            return $this->sendError('Aktuelnost not found');
        }

        $aktuelnost = $this->aktuelnostRepository->update($input, $id);

        return $this->sendResponse($aktuelnost->toArray(), 'aktuelnost updated successfully');
    }

    /**
     * Remove the specified aktuelnost from storage.
     * DELETE /aktuelnosts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var aktuelnost $aktuelnost */
        $aktuelnost = $this->aktuelnostRepository->findWithoutFail($id);

        if (empty($aktuelnost)) {
            return $this->sendError('Aktuelnost not found');
        }

        $aktuelnost->delete();

        return $this->sendResponse($id, 'Aktuelnost deleted successfully');
    }
}
