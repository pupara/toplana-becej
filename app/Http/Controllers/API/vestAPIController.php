<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatevestAPIRequest;
use App\Http\Requests\API\UpdatevestAPIRequest;
use App\Models\vest;
use App\Repositories\vestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class vestController
 * @package App\Http\Controllers\API
 */

class vestAPIController extends AppBaseController
{
    /** @var  vestRepository */
    private $vestRepository;

    public function __construct(vestRepository $vestRepo)
    {
        $this->vestRepository = $vestRepo;
    }

    /**
     * Display a listing of the vest.
     * GET|HEAD /vests
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->vestRepository->pushCriteria(new RequestCriteria($request));
        $this->vestRepository->pushCriteria(new LimitOffsetCriteria($request));
        $vests = $this->vestRepository->all();

        return $this->sendResponse($vests->toArray(), 'Vests retrieved successfully');
    }

    /**
     * Store a newly created vest in storage.
     * POST /vests
     *
     * @param CreatevestAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatevestAPIRequest $request)
    {
        $input = $request->all();

        $vests = $this->vestRepository->create($input);

        return $this->sendResponse($vests->toArray(), 'Vest saved successfully');
    }

    /**
     * Display the specified vest.
     * GET|HEAD /vests/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var vest $vest */
        $vest = $this->vestRepository->findWithoutFail($id);

        if (empty($vest)) {
            return $this->sendError('Vest not found');
        }

        return $this->sendResponse($vest->toArray(), 'Vest retrieved successfully');
    }

    /**
     * Update the specified vest in storage.
     * PUT/PATCH /vests/{id}
     *
     * @param  int $id
     * @param UpdatevestAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatevestAPIRequest $request)
    {
        $input = $request->all();

        /** @var vest $vest */
        $vest = $this->vestRepository->findWithoutFail($id);

        if (empty($vest)) {
            return $this->sendError('Vest not found');
        }

        $vest = $this->vestRepository->update($input, $id);

        return $this->sendResponse($vest->toArray(), 'vest updated successfully');
    }

    /**
     * Remove the specified vest from storage.
     * DELETE /vests/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var vest $vest */
        $vest = $this->vestRepository->findWithoutFail($id);

        if (empty($vest)) {
            return $this->sendError('Vest not found');
        }

        $vest->delete();

        return $this->sendResponse($id, 'Vest deleted successfully');
    }
}
