<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatevestRequest;
use App\Http\Requests\UpdatevestRequest;
use App\Repositories\vestRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class vestController extends AppBaseController
{
    /** @var  vestRepository */
    private $vestRepository;

    public function __construct(vestRepository $vestRepo)
    {
        $this->vestRepository = $vestRepo;
    }

    /**
     * Display a listing of the vest.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->vestRepository->pushCriteria(new RequestCriteria($request));
        $vests = $this->vestRepository->all();

        return view('vests.index')
            ->with('vests', $vests);
    }

    /**
     * Show the form for creating a new vest.
     *
     * @return Response
     */
    public function create()
    {
        return view('vests.create');
    }

    /**
     * Store a newly created vest in storage.
     *
     * @param CreatevestRequest $request
     *
     * @return Response
     */
    public function store(CreatevestRequest $request)
    {
        $input = $request->all();

        $slug = str_slug($request->naslov);
        $input = array_add($input, 'slug', $slug);

        $vest = $this->vestRepository->create($input);

        Flash::success('Vest saved successfully.');

        return redirect(route('vests.index'));
    }

    /**
     * Display the specified vest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $vest = $this->vestRepository->findWithoutFail($id);

        if (empty($vest)) {
            Flash::error('Vest not found');

            return redirect(route('vests.index'));
        }

        return view('vests.show')->with('vest', $vest);
    }

    /**
     * Show the form for editing the specified vest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $vest = $this->vestRepository->findWithoutFail($id);

        if (empty($vest)) {
            Flash::error('Vest not found');

            return redirect(route('vests.index'));
        }

        return view('vests.edit')->with('vest', $vest);
    }

    /**
     * Update the specified vest in storage.
     *
     * @param  int              $id
     * @param UpdatevestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatevestRequest $request)
    {
        $vest = $this->vestRepository->findWithoutFail($id);

        if (empty($vest)) {
            Flash::error('Vest not found');

            return redirect(route('vests.index'));
        }

        $input = $request->all();
        $slug = str_slug($request->naslov);
        $input = array_add($input, 'slug', $slug);

        $vest = $this->vestRepository->update($input, $id);

        Flash::success('Vest updated successfully.');

        return redirect(route('vests.index'));
    }

    /**
     * Remove the specified vest from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $vest = $this->vestRepository->findWithoutFail($id);

        if (empty($vest)) {
            Flash::error('Vest not found');

            return redirect(route('vests.index'));
        }

        $this->vestRepository->delete($id);

        Flash::success('Vest deleted successfully.');

        return redirect(route('vests.index'));
    }
}
