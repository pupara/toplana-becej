<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatenabavkaRequest;
use App\Http\Requests\UpdatenabavkaRequest;
use App\Repositories\nabavkaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class nabavkaController extends AppBaseController
{
    /** @var  nabavkaRepository */
    private $nabavkaRepository;

    public function __construct(nabavkaRepository $nabavkaRepo)
    {
        $this->nabavkaRepository = $nabavkaRepo;
    }

    /**
     * Display a listing of the nabavka.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->nabavkaRepository->pushCriteria(new RequestCriteria($request));
        $nabavkas = $this->nabavkaRepository->all();

        return view('nabavkas.index')
            ->with('nabavkas', $nabavkas);
    }

    /**
     * Show the form for creating a new nabavka.
     *
     * @return Response
     */
    public function create()
    {
        return view('nabavkas.create');
    }

    /**
     * Store a newly created nabavka in storage.
     *
     * @param CreatenabavkaRequest $request
     *
     * @return Response
     */
    public function store(CreatenabavkaRequest $request)
    {

        $input = $request->all();

        if(!empty($request->file('dokument'))){
            $request->file('dokument')->move(public_path('files'), $request->file('dokument')->getClientOriginalName());
            $imageName ='files\\'. $request->file('dokument')->getClientOriginalName();

            $input = array_except($input, ['dokument']);
            $input = array_add($input, 'dokument', $imageName);
        }

        $blog = $this->nabavkaRepository->create($input);

        Flash::success('Nabavka saved successfully.');

        return redirect(route('nabavkas.index'));
    }

    /**
     * Display the specified nabavka.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $nabavka = $this->nabavkaRepository->findWithoutFail($id);

        if (empty($nabavka)) {
            Flash::error('Nabavka not found');

            return redirect(route('nabavkas.index'));
        }

        return view('nabavkas.show')->with('nabavka', $nabavka);
    }

    /**
     * Show the form for editing the specified nabavka.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $nabavka = $this->nabavkaRepository->findWithoutFail($id);

        if (empty($nabavka)) {
            Flash::error('Nabavka not found');

            return redirect(route('nabavkas.index'));
        }

        return view('nabavkas.edit')->with('nabavka', $nabavka);
    }

    /**
     * Update the specified nabavka in storage.
     *
     * @param  int              $id
     * @param UpdatenabavkaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatenabavkaRequest $request)
    {
        $nabavka = $this->nabavkaRepository->findWithoutFail($id);

        if (empty($nabavka)) {
            Flash::error('Nabavka not found');

            return redirect(route('nabavkas.index'));
        }

        $input = $request->all();

        if(!empty($request->file('dokument'))){
            $request->file('dokument')->move(public_path('files'), $request->file('dokument')->getClientOriginalName());
            $imageName ='files\\'. $request->file('dokument')->getClientOriginalName();

            $input = array_except($input, ['dokument']);
            $input = array_add($input, 'dokument', $imageName);
            $blog = $this->nabavkaRepository->update($input, $id);


            Flash::success('Nabavka updated successfully.');

            return redirect(route('nabavkas.index'));
        }

        else{
            $input = array_except($input, ['dokument']);
            $blog = $this->nabavkaRepository->update($input, $id);


            Flash::success('Nabavka updated successfully.');

            return redirect(route('nabavkas.index'));

        }

    }

    /**
     * Remove the specified nabavka from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $nabavka = $this->nabavkaRepository->findWithoutFail($id);

        if (empty($nabavka)) {
            Flash::error('Nabavka not found');

            return redirect(route('nabavkas.index'));
        }

        $this->nabavkaRepository->delete($id);

        Flash::success('Nabavka deleted successfully.');

        return redirect(route('nabavkas.index'));
    }
}
