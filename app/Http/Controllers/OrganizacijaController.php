<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\WeatherTrait;



class OrganizacijaController extends Controller
{
    use WeatherTrait;

    public function show(Request $request)
    {

        $clima=$this->getWeather();

        return view('organizacija')->with('clima', $clima);
    }

}
