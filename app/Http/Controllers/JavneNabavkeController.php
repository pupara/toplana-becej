<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\WeatherTrait;
use App\Models\nabavka;
use App\Repositories\nabavkaRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;




class JavneNabavkeController extends Controller
{
    use WeatherTrait;

    public function __construct(nabavkaRepository $nabavkaRepo)
    {
        $this->nabavkaRepository = $nabavkaRepo;
    }

    public function show(Request $request)
    {
       $this->nabavkaRepository->pushCriteria(new RequestCriteria($request));

       $blogs = $this->nabavkaRepository->nabavke()->paginate(20);
       $clima=$this->getWeather();

        $data = array(
            'blogs'  => $blogs,
            'clima'   => $clima);

        return view('javne-nabavke')->with($data);
    }

    public function showArhiva(Request $request)
    {

        $this->nabavkaRepository->pushCriteria(new RequestCriteria($request));
        $blogs = $this->nabavkaRepository->arhiva()->paginate(20);

        $clima=$this->getWeather();

        $data = array(
            'blogs'  => $blogs,
            'clima'   => $clima);

        return view('arhiva')->with($data);
    }
    public function showIzvestaj(Request $request)
    {

        $this->nabavkaRepository->pushCriteria(new RequestCriteria($request));
        $blogs = $this->nabavkaRepository->izvestaj()->paginate(20);

        $clima=$this->getWeather();

        $data = array(
            'blogs'  => $blogs,
            'clima'   => $clima);

        return view('finansijski-izvestaji')->with($data);
    }

}
