<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\WeatherTrait;
use App\Repositories\vestRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\vest;
use Illuminate\Support\Facades\DB;



class VestiController extends Controller
{
    use WeatherTrait;

    public function __construct(vestRepository $nabavkaRepo)
    {
        $this->vestRepository = $nabavkaRepo;
    }



    public function show(Request $request)
    {
        $this->vestRepository->pushCriteria(new RequestCriteria($request));

        $blogs = $this->vestRepository->vest()->paginate(20);
        $clima=$this->getWeather();

        $data = array(
            'blogs'  => $blogs,
            'clima'   => $clima);

        return view('vesti')->with($data);
    }

    public function showVesti(Request $request)
    {
        $this->vestRepository->pushCriteria(new RequestCriteria($request));

        $blogs = $this->vestRepository->aktuelnost()->paginate(20);
        $clima=$this->getWeather();

        $data = array(
            'blogs'  => $blogs,
            'clima'   => $clima);

        return view('aktuelnost')->with($data);
    }

    public function showAktuelnosti(Request $request)
    {
        $this->vestRepository->pushCriteria(new RequestCriteria($request));

        $blogs = $this->vestRepository->aktuelnost()->paginate(20);
        $clima=$this->getWeather();

        $data = array(
            'blogs'  => $blogs,
            'clima'   => $clima);

        return view('aktuelnost')->with($data);
    }

    public function vestislug($slug)
    {
        $blog= vest::where('slug',$slug)->first();

        $clima=$this->getWeather();

        $data = array(
            'blog'  => $blog,
            'clima'   => $clima);

        return view('vestSingle')->with($data);
    }

    public function aktuelnostiSlug($slug)
    {
        $blog= vest::where('slug',$slug)->first();

        $clima=$this->getWeather();

        $data = array(
            'blog'  => $blog,
            'clima'   => $clima);

        return view('aktuelnostSlug')->with($data);
    }


}
