<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreategrejnaRequest;
use App\Http\Requests\UpdategrejnaRequest;
use App\Repositories\grejnaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class grejnaController extends AppBaseController
{
    /** @var  grejnaRepository */
    private $grejnaRepository;

    public function __construct(grejnaRepository $grejnaRepo)
    {
        $this->grejnaRepository = $grejnaRepo;
    }

    /**
     * Display a listing of the grejna.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->grejnaRepository->pushCriteria(new RequestCriteria($request));
        $grejnas = $this->grejnaRepository->all();

        return view('grejnas.index')
            ->with('grejnas', $grejnas);
    }

    /**
     * Show the form for creating a new grejna.
     *
     * @return Response
     */
    public function create()
    {
        return view('grejnas.create');
    }

    /**
     * Store a newly created grejna in storage.
     *
     * @param CreategrejnaRequest $request
     *
     * @return Response
     */
    public function store(CreategrejnaRequest $request)
    {
        $input = $request->all();

        $grejna = $this->grejnaRepository->create($input);

        Flash::success('Grejna saved successfully.');

        return redirect(route('grejnas.index'));
    }

    /**
     * Display the specified grejna.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $grejna = $this->grejnaRepository->findWithoutFail($id);

        if (empty($grejna)) {
            Flash::error('Grejna not found');

            return redirect(route('grejnas.index'));
        }

        return view('grejnas.show')->with('grejna', $grejna);
    }

    /**
     * Show the form for editing the specified grejna.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $grejna = $this->grejnaRepository->findWithoutFail($id);

        if (empty($grejna)) {
            Flash::error('Grejna not found');

            return redirect(route('grejnas.index'));
        }

        return view('grejnas.edit')->with('grejna', $grejna);
    }

    /**
     * Update the specified grejna in storage.
     *
     * @param  int              $id
     * @param UpdategrejnaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdategrejnaRequest $request)
    {
        $grejna = $this->grejnaRepository->findWithoutFail($id);

        if (empty($grejna)) {
            Flash::error('Grejna not found');

            return redirect(route('grejnas.index'));
        }

        $grejna = $this->grejnaRepository->update($request->all(), $id);

        Flash::success('Grejna updated successfully.');

        return redirect(route('grejnas.index'));
    }

    /**
     * Remove the specified grejna from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $grejna = $this->grejnaRepository->findWithoutFail($id);

        if (empty($grejna)) {
            Flash::error('Grejna not found');

            return redirect(route('grejnas.index'));
        }

        $this->grejnaRepository->delete($id);

        Flash::success('Grejna deleted successfully.');

        return redirect(route('grejnas.index'));
    }
}
