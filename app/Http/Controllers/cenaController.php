<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecenaRequest;
use App\Http\Requests\UpdatecenaRequest;
use App\Repositories\cenaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cenaController extends AppBaseController
{
    /** @var  cenaRepository */
    private $cenaRepository;

    public function __construct(cenaRepository $cenaRepo)
    {
        $this->cenaRepository = $cenaRepo;
    }

    /**
     * Display a listing of the cena.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->cenaRepository->pushCriteria(new RequestCriteria($request));
        $cenas = $this->cenaRepository->all();

        return view('cenas.index')
            ->with('cenas', $cenas);
    }

    /**
     * Show the form for creating a new cena.
     *
     * @return Response
     */
    public function create()
    {
        return view('cenas.create');
    }

    /**
     * Store a newly created cena in storage.
     *
     * @param CreatecenaRequest $request
     *
     * @return Response
     */
    public function store(CreatecenaRequest $request)
    {
        $input = $request->all();

        $cena = $this->cenaRepository->create($input);

        Flash::success('Cena saved successfully.');

        return redirect(route('cenas.index'));
    }

    /**
     * Display the specified cena.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cena = $this->cenaRepository->findWithoutFail($id);

        if (empty($cena)) {
            Flash::error('Cena not found');

            return redirect(route('cenas.index'));
        }

        return view('cenas.show')->with('cena', $cena);
    }

    /**
     * Show the form for editing the specified cena.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cena = $this->cenaRepository->findWithoutFail($id);

        if (empty($cena)) {
            Flash::error('Cena not found');

            return redirect(route('cenas.index'));
        }

        return view('cenas.edit')->with('cena', $cena);
    }

    /**
     * Update the specified cena in storage.
     *
     * @param  int              $id
     * @param UpdatecenaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecenaRequest $request)
    {
        $cena = $this->cenaRepository->findWithoutFail($id);

        if (empty($cena)) {
            Flash::error('Cena not found');

            return redirect(route('cenas.index'));
        }

        $cena = $this->cenaRepository->update($request->all(), $id);

        Flash::success('Cena updated successfully.');

        return redirect(route('cenas.index'));
    }

    /**
     * Remove the specified cena from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cena = $this->cenaRepository->findWithoutFail($id);

        if (empty($cena)) {
            Flash::error('Cena not found');

            return redirect(route('cenas.index'));
        }

        $this->cenaRepository->delete($id);

        Flash::success('Cena deleted successfully.');

        return redirect(route('cenas.index'));
    }
}
