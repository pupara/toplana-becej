<?php

namespace App\Http\Requests\API;

use App\Models\aktuelnost;
use InfyOm\Generator\Request\APIRequest;

class CreateaktuelnostAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return aktuelnost::$rules;
    }
}
