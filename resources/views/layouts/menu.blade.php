<li class="{{ Request::is('nabavkas*') ? 'active' : '' }}">
    <a href="{!! route('nabavkas.index') !!}"><i class="fa fa-edit"></i><span>Dokumenti</span></a>
</li>

<li class="{{ Request::is('vests*') ? 'active' : '' }}">
    <a href="{!! route('vests.index') !!}"><i class="fa fa-edit"></i><span>Vesti / Aktuelnosti</span></a>
</li>


<li class="{{ Request::is('cenas*') ? 'active' : '' }}">
    <a href="{!! route('cenas.index') !!}"><i class="fa fa-edit"></i><span>Cena toplotne energije</span></a>
</li>

<li class="{{ Request::is('brojilos*') ? 'active' : '' }}">
    <a href="{!! route('brojilos.index') !!}"><i class="fa fa-edit"></i><span>Ocitavanje meraca</span></a>
</li>

<li class="{{ Request::is('grejnas*') ? 'active' : '' }}">
    <a href="{!! route('grejnas.index') !!}"><i class="fa fa-edit"></i><span>Grejna sezona</span></a>
</li>

