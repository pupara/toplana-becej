<table class="table table-responsive" id="brojilos-table">
    <thead>
        <th>Naslov</th>
        <th>Tekst</th>
        <th>Slika</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($brojilos as $brojilo)
        <tr>
            <td>{!! $brojilo->naslov !!}</td>
            <td>{!! $brojilo->tekst !!}</td>
            <td>{!! $brojilo->slika !!}</td>
            <td>
                {!! Form::open(['route' => ['brojilos.destroy', $brojilo->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('brojilos.show', [$brojilo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('brojilos.edit', [$brojilo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>