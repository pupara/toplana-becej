<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $brojilo->id !!}</p>
</div>

<!-- Naslov Field -->
<div class="form-group">
    {!! Form::label('naslov', 'Naslov:') !!}
    <p>{!! $brojilo->naslov !!}</p>
</div>

<!-- Tekst Field -->
<div class="form-group">
    {!! Form::label('tekst', 'Tekst:') !!}
    <p>{!! $brojilo->tekst !!}</p>
</div>

<!-- Slika Field -->
<div class="form-group">
    {!! Form::label('slika', 'Slika:') !!}
    <img src="{{ asset($brojilo->slika) }}"></img>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $brojilo->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $brojilo->updated_at !!}</p>
</div>

