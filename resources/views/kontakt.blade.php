<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="Toplana Bečej" />
    <meta name="description" content="Toplana Bečej">
    <meta name="author" content="www.fuzzy-panda.com">

    <title>Toplana Bečej</title>

    <!-- Retina Bookmark Icon -->
    <link rel="apple-touch-icon-precomposed" href="apple-icon.png" />

    <!-- Reset CSS -->
    <link href="{{asset('css/reset.css')}}" rel="stylesheet" />

    <!-- CSS -->
    <link href="{{asset('css/whhg.css')}}" rel="stylesheet" />
    <link href="{{asset('css/foundstrap.css')}}" rel="stylesheet" />

    <!-- CSS Plugin -->
    <link href="{{asset('css/mediaplayer.css')}}" rel="stylesheet" />
    <link href="{{asset('js/rs-plugin/css/settings.css')}}" rel="stylesheet" media="screen" />

    <!-- epicon stylesheet -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('css/epicon-responsive.css')}}" rel="stylesheet" />

    <!-- Theme Option -->
    <link href="{{asset('css/theme-switcher.css')}}" rel="stylesheet" />
    <link href="{{asset('css/theme/blue.css')}}" rel="stylesheet" />
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" />


    <script src="{{asset('js/modernizr.js')}}"></script>

</head>
<body style="background: url({{asset('css/theme/pattern/pattern4.png')}}) repeat;">

<!-- main-container -->
<div id="main-container" class="box">

    <!-- headaer -->
@include('partials/header')

<!-- header end here -->

    <!-- slider here -->
    <section class="container" id="page-header">
        <div class="row">
            <div class="inner-container">
                <div class="large-12 column text-right epic-animate" data-animate="fadeInDown">
                    <h1>Kontakt</h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Kontakt</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- slider end here -->
    <section class="container no-padding-bottom">
        <div class="row">
            <div class="large-12 column">
                <div class="map-container epic-animate" data-animate="fadeIn">
                    <div id="map">
                        <h3>Google Map</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="container no-padding" id="contact-information">
        <div class="row">
            <div class="inner-container">
                <div class="large-6 medium-6 medium-potrait-12 small-12 column epic-animate" data-animate="fadeInDown">
                    <div class="inner-column">
                        <h3><strong class="epicon-strong">Kontakt</strong></h3>
                      <span><strong>Osnovni podaci</strong> </span>
                        <ul class="no-bullet contact-info">
                            <li><i class="icon-map-marker"></i> Petrovselski put 3, 21220 Bečej</li>
                            <li><i class="icon-envelope"></i> office@toplanabecej.rs</li>
                            <li><i class="icon-briefcase"></i> PIB: 100435109</li>
                            <li><i class="icon-briefcase"></i> MB: 08161534</li>
                            <li><i class="icon-briefcase"></i> ŠD: 3530</li>

                        </ul>
                        <span><strong>Kontakt podaci</strong> </span>
                        <ul class="no-bullet contact-info">
                            <li><i class="icon-phonealt"></i> Centrala: 021/6912 761</li>
                            <li><i class="icon-callalt"></i><span class="red2"> Dežurna služba: 021/6915-470</span></li>
                            <li><i class="icon-copy"></i> Fax: 021/6911-390</li>
                        </ul>
                        <span><strong>Podaci o tekućim računima u bankama</strong> </span>
                        <ul class="no-bullet contact-info">
                            <li><i class="icon-briefcase"></i>BANCA INTESA: 160-9709-49</li>
                            <li><i class="icon-briefcase"></i>VOJVOĐANSKA BANKA: 355-1046796-18</li>
                            <li><i class="icon-briefcase"></i>ERSTE BANK: 340-24866-22</li>
                            <li><i class="icon-briefcase"></i>HYPO ALPE-ADRIA-BANK : 165-9480-26</li>
                            <li><i class="icon-briefcase"></i>AIK BANKA: 105-31340-02</li>
                            <li><i class="icon-briefcase"></i>UNICREDIT BANK: 170-003002523800087</li>
                            <li><i class="icon-briefcase"></i>ERSTE BANK : 340-0000000024866-22</li>
                        </ul>

                    </div>
                </div>

                <div class="large-6 medium-6 medium-potrait-12 small-12 column no-padding epic-animate" data-animate="fadeInDown" style="margin-top: 150px">
                    <div class="inner-column" id="contact-form">
                        <form class="contact-form clearfix">

                            <div class="alert-box green success-contact">
                                <i class="icon-circleselect"></i>
                                Uspešno ste poslali poruku, potrudićemo se da Vam odgovorimo u najkraćem mogućem roku!
                            </div>

                            <div class="input-group large-12 column">
                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                <input type="text" name="name" id="name" class="form-control" placeholder="Vaše ime i prezime">
                            </div>

                            <div class="input-group large-12 column">
                                <span class="input-group-addon"><i class="icon-envelope"></i></span>
                                <input type="email" name="email" id="email" class="form-control" placeholder="Vaša email adresa">
                            </div>

                            <div class="input-group large-12 column">
                                <span class="input-group-addon"><i class="icon-mailinglists"></i></span>
                                <input type="text" name="subject" id="subject" class="form-control" placeholder="Vrsta upita">
                            </div>

                            <div class="form-group large-12 column">
                                <textarea name="message" id="message" class="form-control" rows="5" placeholder="Vaša poruka"></textarea>
                            </div>

                            <div class="form-group large-12 column">
                                <a class="button small button-group" id="buttonsend" href="#">Pošaljite poruku
                                    <span><i class="icon-chevron-right"></i></span>
                                </a>
                                <span class="loading"></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- footer -->
@include('partials/footer')
<!-- footer end here -->

</div>
<!-- main-container end here -->

<!-- javascript -->
<script src="js/jquery.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.smartmenus.min.js"></script>
<script src="js/jquery.scrollUp.js"></script>
<script src="js/jquery.retina.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/icheck.js"></script>

<!-- javascript plugin -->
<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/jquery.player.js"></script>

<!-- javascript core -->
<script src="js/theme-script.js"></script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyBsum59r_4Wa2Phlaac8lx8zLFICBbonyo"></script>
<script src="js/jquery.maps.js"></script>
<script src="js/contact-form.js"></script>


<script type="text/javascript">
    jQuery(document).ready(function($){
        var map = new GMaps({
            el: '#map',
            lat: 45.622119,
            lng: 20.035874,
            zoom: 15,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: false,
            zoomControl : true,
            zoomControlOpt: {
                style : 'SMALL',
                position: 'TOP_LEFT'
            },
            panControl : false,
            streetViewControl : false,
            mapTypeControl: false,
            overviewMapControl: false
        });

        map.addMarker({
            lat: 45.622119,
            lng: 20.035874,
            icon: "img/map-marker.png"
        });

        var styles = [{"featureType":"road","elementType":"labels","stylers":[{"visibility":"simplified"},{"lightness":20}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#a1cdfc"},{"saturation":30},{"lightness":49}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"hue":"#f49935"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"hue":"#fad959"}]}];

        map.addStyle({
            styledMapName:"Styled Map",
            styles: styles,
            mapTypeId: "map_style"
        });

        map.setStyle("map_style");
    });
</script>
</body>
</html>
