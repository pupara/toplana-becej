<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $vest->id !!}</p>
</div>

<!-- Naslov Field -->
<div class="form-group">
    {!! Form::label('naslov', 'Naslov:') !!}
    <p>{!! $vest->naslov !!}</p>
</div>

<!-- Tekst Field -->
<div class="form-group">
    {!! Form::label('tekst', 'Tekst:') !!}
    <p>{!! $vest->tekst !!}</p>
</div>

<div class="form-group">
    {!! Form::label('tip', 'Tip:') !!}
    <p>{!! $vest->tip !!}</p>
</div>


<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $vest->slug !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $vest->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $vest->updated_at !!}</p>
</div>

