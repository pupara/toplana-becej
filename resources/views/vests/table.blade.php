<table class="table table-responsive" id="vests-table">
    <thead>
        <th>Naslov</th>
        <th>Tekst</th>
        <th>Tip</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($vests as $vest)
        <tr>
            <td>{!! $vest->naslov !!}</td>
            <td>{{ str_limit( strip_tags($vest->tekst), 175)}}</td>
            <td>{!! $vest->tip !!}</td>
            <td>
                {!! Form::open(['route' => ['vests.destroy', $vest->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('vests.show', [$vest->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('vests.edit', [$vest->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>