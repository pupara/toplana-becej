@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cena toplotne energije
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($cena, ['route' => ['cenas.update', $cena->id], 'method' => 'patch']) !!}

                        @include('cenas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection