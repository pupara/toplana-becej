<!-- Naslov Field -->
<div class="form-group col-sm-6">
    {!! Form::label('naslov', 'Naslov:') !!}
    {!! Form::text('naslov', null, ['class' => 'form-control']) !!}
</div>

<!-- Tekst Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('tekst', 'Tekst:') !!}
    {!! Form::textarea('tekst', null, array('id'=> 'editor1','class' => 'form-control')) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cenas.index') !!}" class="btn btn-default">Cancel</a>
</div>
