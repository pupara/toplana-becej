<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $cena->id !!}</p>
</div>

<!-- Naslov Field -->
<div class="form-group">
    {!! Form::label('naslov', 'Naslov:') !!}
    <p>{!! $cena->naslov !!}</p>
</div>

<!-- Tekst Field -->
<div class="form-group">
    {!! Form::label('tekst', 'Tekst:') !!}
    <p>{!! $cena->tekst !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $cena->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $cena->updated_at !!}</p>
</div>

