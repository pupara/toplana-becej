<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="Toplana Bečej" />
    <meta name="description" content="Toplana Bečej">
    <meta name="author" content="www.fuzzy-panda.com">

    <title>Toplana Bečej</title>

    <!-- Retina Bookmark Icon -->
    <link rel="apple-touch-icon-precomposed" href="apple-icon.png" />

    <!-- Reset CSS -->
    <link href="{{asset('css/reset.css')}}" rel="stylesheet" />

    <!-- CSS -->
    <link href="{{asset('css/whhg.css')}}" rel="stylesheet" />
    <link href="{{asset('css/foundstrap.css')}}" rel="stylesheet" />

    <!-- CSS Plugin -->
    <link href="{{asset('css/mediaplayer.css')}}" rel="stylesheet" />
    <link href="{{asset('js/rs-plugin/css/settings.css')}}" rel="stylesheet" media="screen" />

    <!-- epicon stylesheet -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('css/epicon-responsive.css')}}" rel="stylesheet" />

    <!-- Theme Option -->
    <link href="{{asset('css/theme-switcher.css')}}" rel="stylesheet" />
    <link href="{{asset('css/theme/blue.css')}}" rel="stylesheet" />
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" />


    <script src="{{asset('js/modernizr.js')}}"></script>

</head>
<body style="background: url({{asset('css/theme/pattern/pattern4.png')}}) repeat;">

<!-- main-container -->
<div id="main-container" class="box">

    <!-- headaer -->
@include('partials/header')

<!-- header end here -->

    <!-- slider here -->
    <section class="container" id="page-header">
        <div class="row">
            <div class="inner-container">
                <div class="large-12 column text-right epic-animate" data-animate="fadeInDown">
                    <h1>Aktuelnosti<span class="epicon-color"></span></h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Aktuelnosti</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- slider end here -->


    <section class="container">
        <div class="row">
            <div class="large-12column">

                <article class="epic-animate" data-animate="fadeInDown">
                    <div class="blog-info">

                        <div class="date-blog-info">{{ Carbon\Carbon::parse($blog->created_at)->format('d.m') }}<small>{{ Carbon\Carbon::parse($blog->created_at)->format('Y') }}</small></div>
                    </div>
                    <div class="blog-container">

                        <h3>{!! $blog->naslov !!}</h3>


                        <p>
                            {!!$blog->tekst !!}
                        </p>


                    </div>
                </article>
            </div>
        </div>
    </section>
    <!-- footer -->
@include('partials/footer')
<!-- footer end here -->

</div>
<!-- main-container end here -->

<!-- javascript -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/jquery.fancybox.js')}}"></script>
<script src="{{asset('js/jquery.fancybox-media.js')}}"></script>
<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('js/jquery.smartmenus.min.js')}}"></script>
<script src="{{asset('js/jquery.scrollUp.js')}}"></script>
<script src="{{asset('js/jquery.retina.js')}}"></script>
<script src="{{asset('js/jquery.cookie.js')}}"></script>
<script src="{{asset('js/icheck.js')}}"></script>

<!-- javascript plugin -->
<script src="{{asset('js/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('js/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{asset('js/jquery.countTo.js')}}"></script>
<script src="{{asset('js/jquery.player.js')}}"></script>

<!-- javascript core -->
<script src="{{asset('js/theme-script.js')}}"></script>
<script src="{{asset('js/twitter/jquery.tweet.js')}}"></script>

</body>
</html>
