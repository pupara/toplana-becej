<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $aktuelnost->id !!}</p>
</div>

<!-- Naslov Field -->
<div class="form-group">
    {!! Form::label('naslov', 'Naslov:') !!}
    <p>{!! $aktuelnost->naslov !!}</p>
</div>

<!-- Tekst Field -->
<div class="form-group">
    {!! Form::label('tekst', 'Tekst:') !!}
    <p>{!! $aktuelnost->tekst !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $aktuelnost->slug !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $aktuelnost->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $aktuelnost->updated_at !!}</p>
</div>

