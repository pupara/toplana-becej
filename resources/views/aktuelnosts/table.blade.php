<table class="table table-responsive" id="aktuelnosts-table">
    <thead>
        <th>Naslov</th>
        <th>Tekst</th>
        <th>Slug</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($aktuelnosts as $aktuelnost)
        <tr>
            <td>{!! $aktuelnost->naslov !!}</td>
            <td>{{ str_limit( strip_tags($aktuelnost->tekst), 175)}}</td>
            <td>{!! $aktuelnost->slug !!}</td>
            <td>
                {!! Form::open(['route' => ['aktuelnosts.destroy', $aktuelnost->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('aktuelnosts.show', [$aktuelnost->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('aktuelnosts.edit', [$aktuelnost->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>