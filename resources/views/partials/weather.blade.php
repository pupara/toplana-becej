<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="weather, world, openweathermap, weather, layer" />
    <meta name="description" content="A layer with current weather conditions in cities for world wide" />
    <meta name="domain" content="openweathermap.org" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
</head>
<body>
<div style="font-size: medium; font-weight: bold; margin-bottom: 0px;">Bečej</div>
<div style="float: left; width: 130px;">
    <div style="display: block; clear: left;">
        <div style="float: left;" title="Titel">
            <img height="45" width="45" style="border: medium none; width: 45px; height: 45px; background: url(http://openweathermap.org/img/w/{!! $clima->weather[0]->icon !!}.png) repeat scroll 0% 0% transparent;" alt="title" src="http://openweathermap.org/images/transparent.png"/>
        </div>
        <div style="float: left;">
            <div style="display: block; clear: left; font-size: medium; font-weight: bold; padding: 0pt 3pt;" title="Trenutna temperatura">{!! $clima->main->temp !!} °c</div>
            <div style="display: block; width: 85px; overflow: visible;"></div>
        </div>
    </div>
    <div style="display: block; clear: left; color: gray; font-size: x-small;" >Vlaznost: {!! $clima->main->humidity !!}%</div>
    <div style="display: block; clear: left; color: gray; font-size: x-small;" >Vetar: {!! $clima->wind->speed !!} m/s</div>
    <div style="display: block; clear: left; color: gray; font-size: x-small;" >Pritisak: {!! $clima->main->pressure !!} hpa</div>
</div>
<div style="display: block; clear: left; color: gray; font-size: x-small;">
    <a href="http://openweathermap.org/city/792814?utm_source=openweathermap&utm_medium=widget&utm_campaign=html_old" target="_blank">Više..</a>
</div>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-31601618-9', 'auto');ga('send', 'pageview');</script>
</body>
</html>