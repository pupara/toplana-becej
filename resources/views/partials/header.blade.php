<header>
    <div class="row">
        <div class="large-12 column ">
            <div class="header-info blue ">
                <ul class="left-info no-bullet inline-list" style="color: white;">
                    <li><i class="icon-phonealt"></i>  021 6912 761</li>
                    <li><i class="icon-envelope"></i> office@toplanabecej.rs</li>
                    <li><i class="icon-callalt"></i><span style="color: #8ffb20;" ">TELEFON ZA HITNE INTERVENCIJE 021/6915-470</span></li>

                </ul>

                <ul class="right-info no-bullet inline-list">

                    <li class="language-flag">
                        <a href="#"><img src="{{asset('img/cust/srbija.png')}}" alt="Srbija"></a>
                        <a href="#"><img src="{{asset('img/cust/vojvodina.png')}}" alt="Vojvodina"></a>

                    </li>
                </ul>
            </div>

            <div class="header-container azure">
                <div class="logo-container">
                    <a href="{{route('home')}}">
                        <img class="retina" style="margin-bottom: 5px;" src="{{asset('img/logo2.png')}}" alt="Logo"><strong class="black" style="font-size:28px;letter-spacing: 1px;"> JP Toplana Bečej</strong>
                    </a>
                </div>

                <!-- menu navigation -->
                <nav class="menu-container azure">
                    <ul id="menu" class="sm epicon-menu azure">
                        <li {!! (Route::is('home')? 'class="active"':"") !!}><a href="{{route('home')}}">Početna</a></li>
                        <li><a href="#">O nama</a>
                            <ul>
                                <li {!! (Route::is('istorijat')? 'class="active"':"") !!}><a href="{{route('istorijat')}}">Istorijat i Organizacija</a></li>
                                <li {!! (Route::is('organizacija')? 'class="active"':"") !!}><a href="{{route('tehnicki-podaci')}}">Tehnički podaci</a></li>
                            </ul>
                        </li>
                        <li {!! (Route::is('vesti')? 'class="active"':"") !!}><a href="{{route('vesti')}}">Vesti</a>
                        <li {!! (Route::is('aktuelnosti')? 'class="active"':"") !!}><a href="{{route('aktuelnosti')}}">Aktuelnosti</a></li>


                        <li><a href="#">Korisnički servis</a>
                            <ul>
                                <li {!! (Route::is('korisnicki-servis')? 'class="active"':"") !!}><a href="{{route('korisnicki-servis')}}#cenatoplotneenergije">Cena toplotne energije</a></li>
                                <li {!! (Route::is('korisnicki-servis')? 'class="active"':"") !!}><a href="{{route('korisnicki-servis')}}#ocitavanjebrojila">Očitavanje brojila</a></li>
                                <li {!! (Route::is('korisnicki-servis')? 'class="active"':"") !!}><a href="{{route('korisnicki-servis')}}#grejnasezona">Grejna sezona</a></li>
                            </ul>
                        </li>
                        <li {!! (Route::is('javne-nabavke')? 'class="active"':"") !!}><a href="{{route('javne-nabavke')}}">Javne nabavke</a></li>
                        <li><a href="#">Dokumenti</a>
                            <ul>
                                <li {!! (Route::is('finansijski-izvestaji')? 'class="active"':"") !!}><a href="{{route('finansijski-izvestaji')}}">Finansijski izveštaji</a></li>
                                <li {!! (Route::is('arhiva')? 'class="active"':"") !!}><a href="{{route('arhiva')}}">Odluke</a></li>

                            </ul>
                        </li>
                        <li {!! (Route::is('kontakt')? 'class="active"':"") !!}><a href="{{route('kontakt')}}">Kontakt</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
