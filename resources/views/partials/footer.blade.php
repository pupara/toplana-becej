<footer>
    <div class="row ">
        <div class="large-5 medium-12 column">
            <div class="column one-fourth">

                <!-- Text Area -->
                <p><h4>@include('partials/weather')
                </h4> </p>
            </div>

        </div>

        <div class="large-2 medium-4 column">
            <h5 class="title-footer"><i class="icon-map-marker"></i> Adresa</h5>
            <address>
                Petrovselski put 3<br>
                21220 Bečej
            </address>
        </div>

        <div class="large-2 medium-4 column">
            <h5 class="title-footer"><i class="icon-map"></i> Mapa sajta</h5>
            <ul class="no-bullet">
                <li><a href="{{route('home')}}">Početna</a></li>
                <li><a href="{{route('istorijat')}}">Istorijat i organizaciona struktura</a></li>
                <li><a href="{{route('korisnicki-servis')}}">Korisnički servis</a></li>
                <li><a href="{{route('javne-nabavke')}}">Javne nabavke</a></li>
                <li><a href="{{route('finansijski-izvestaji')}}">Finansijski izveštaji</a></li>
                <li><a href="{{route('kontakt')}}">Kontakt</a></li>
            </ul>
        </div>

        <div class="large-2 medium-4 column">
            <h5 class="title-footer"><i class="icon-emailalt"></i> Kontaktirajte nas</h5>
            <ul class="no-bullet">
                <li>Tel: 021 6912 761</li>
                <li>Email: office@toplanabecej.rs </li>
            </ul>
        </div>
    </div>

    <div class="footer-info blue">
        <div class="row">
            <div class="large-5 medium-5 medium-potrait-12 small-12 column">
                <div>
                    Sva prava su zadržana @<a  href="#"> JP Toplana Bečej</a>
                </div>
            </div>

        </div>
    </div>
</footer>