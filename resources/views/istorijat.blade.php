<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="Toplana Bečej" />
    <meta name="description" content="Toplana Bečej">
    <meta name="author" content="www.fuzzy-panda.com">

    <title>Toplana Bečej</title>

    <!-- Retina Bookmark Icon -->
    <link rel="apple-touch-icon-precomposed" href="apple-icon.png" />

    <!-- Reset CSS -->
    <link href="{{asset('css/reset.css')}}" rel="stylesheet" />

    <!-- CSS -->
    <link href="{{asset('css/whhg.css')}}" rel="stylesheet" />
    <link href="{{asset('css/foundstrap.css')}}" rel="stylesheet" />

    <!-- CSS Plugin -->
    <link href="{{asset('css/mediaplayer.css')}}" rel="stylesheet" />
    <link href="{{asset('js/rs-plugin/css/settings.css')}}" rel="stylesheet" media="screen" />

    <!-- epicon stylesheet -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('css/epicon-responsive.css')}}" rel="stylesheet" />

    <!-- Theme Option -->
    <link href="{{asset('css/theme-switcher.css')}}" rel="stylesheet" />
    <link href="{{asset('css/theme/blue.css')}}" rel="stylesheet" />
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" />


    <script src="{{asset('js/modernizr.js')}}"></script>

</head>
<body style="background: url({{asset('css/theme/pattern/pattern4.png')}}) repeat;">

<!-- main-container -->
<div id="main-container" class="box">

    <!-- headaer -->
@include('partials/header')

<!-- header end here -->

    <!-- slider here -->

    <!-- slider end here -->
    <section class="container" id="page-header">
        <div class="row">
            <div class="inner-container">
                <div class="large-12 column text-right epic-animate" data-animate="fadeInDown">
                    <h1 class"white">Istorijat i organizaciona struktura</h1>
                    <ol class="breadcrumb">
                        <li><a href="#">O nama</a></li>
                        <li class="">Istorijat i organizaciona struktura</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="container">
    <div class="row">
        <div class="large-12 medium-12 column epic-animate " data-animate="fadeInDown">
            <div class="panel border-no" id="istorijat">
            <h3 class="gap" data-gap-top="0"><strong class="epicon-strong red">Istorijat</strong></h3>
            <p>
                Javno preduzeće za proizvodnju i isporuku toplotne energije "Toplana" Bečej (u daljem tekstu Toplana), osnovana je 1984. godine, dok je proizvodnju i isporuku toplotne energije otpočela oktobra 1986. godine. Toplana obavlja delatnost proizvodnje i distribucije toplotne energije iz daljinskog centralizovanog izvora kapaciteta 2 x 9,3 MW za grejanje stanova i poslovnih prostorija, vrelovodnom mrežom dužine oko 23 kmTr. Kao energent koristi prirodni gas.<p>
                <p>    Toplana trenutno zapošljava 18 radnika ima 1095 kupaca.Većinu korisnika su kupci iz kategorije domaćinstva, stanari kolektivnih stambenih zgrada i vlasnici individualnih porodičnih objekata, kojih ima 1.016, dok poslovnih objekata i objekata javnih službi ima oko 79. Ukupna instalisana snaga konzuma je 19,3 MW, a ukupna površina koja se greje je oko 114.550m².</p>
        </div>
            <div class="panel border-no">
                <h3 class="gap" data-gap-top="0"><strong class="epicon-strong red">Delatnost</strong></h3>
                <p>
                    Toplana je tokom 2006. godine ispunila sve uslove za licenciranje svoje delatnosti shodno odredbama Zakona o energetici, na osnovu čega je dana 27.07.2006. godine dobila, sa rokom važenja od deset godina, četiri licence.
                </p>
                <p>Međutim, u skladu sa novim Zakonom o energetici i ispunjenja svih uslova za obavljanje energetske delatnosti Toplana je 2016.godine dobila, sa rokom važenja od deset godina tri licence za obavljanje energetske delatnosti:
                </p>
                <ul style="margin-left: 40px">
                    <li>Proizvodnja toplotne energije

                       </li>
                    <li>Distribucija toplotne energije</li>
                    <li>Snabdevanje toplotnom energijom</li>

                </ul>
            </div>
            <div class="panel border-no">
                <h3 class="gap" data-gap-top="0"><strong class="epicon-strong red">Organizacija</strong></h3>
                <p>
                    U cilju što potpunijeg iskorišćenja sredstava rada i postizanja što povoljnijih rezultata rada unutrašnja organizacija rada u Toplani uređuje se tako da se obezbedi skladno obavljanje delatnosti kao i optimalna hijerarhijska postavljenost koja obezbeđuje efikasno rukovođenje i

                    upravljanje. Vršenje delatnosti Toplana obavlja u tri posebne organizacione celine, pod sledećim nazivima:
                </p>
                <ul style="margin-left: 40px">
                    <li>SLUŽBA PROIZVODNJE, DISTRIBUCIJE, SNABDEVANJA I RAZVOJA TEHNIČKOG SISTEMA</li>
                    <li>RAČUNOVODSTVENO FINANSIJSKA SLUŽBA</li>
                    <li>SLUŽBA ZA OPŠTE POSLOVE.</li>
                </ul>
                <p>
                    U okviru Službe proizvodnje, distribucije, snabdevanja i razvoja tehničkog sistema obavljaju se poslovi proizvodnje toplotne energije, vrši se analiza utroška energenata u cilju postizanja što veće racionalnosti proizvodnje, vrši se održavanje svih elemenata toplotnog izvora i primenjuju se propisane mere koje omogućavaju bezbedno i sigurno odvijanje procesa proizvodnje uz njegovo stalno usavršavanje i unapređenje, obavljaju se poslovi na održavanju ispravnosti distributivne mreže i toplotnih podstanica, uključujući i merenje utroška isporučene toplotne energije i regulaciju sistema daljinskog grejanja, vrše se svi poslovi u vezi proširenja toplotnog konzuma, obavljaju se poslovi vezani za blagovremeni obračun utroška toplotne energije i rešavaju odnosi sa neposrednim korisnicima toplote, vrši se stalno usavršavanje i unapređenje celokupnog procesa distribucije toplotne energije i drugi poslovi.
                </p>
                <p>
                    U okviru Računovodstveno finansijske službe obavljaju se poslovi vođenja evidencija svih nastalih promena na sredstvima i izvorima sredstava Toplane, vrši se priprema podataka za poslovne odluke, vrše se poslovi vezani za finansiranje proizvodnje i distribucije toplotne energije, kontroliše se materijalno-finansijska dokumentacija i izrađuju podloge za izveštaje o poslovanju i obavljaju se i drugi poslovi.
                </p>
                <p>
                    U okviru Službe za opšte poslove obavljaju se svi opšti poslovi, administrativno tehnički poslovi vezani za sve službe preduzeća, vodi delovodni protokol, vrši se distribucija pošte, prijem stranaka i drugi poslovi.
                </p>
                <p>
                    Poslovodnu funkciju u Toplani vrši direktor koji organizuje i rukovodi čitavim procesom rada i poslovanja.
                </p>
                <p>
                    Najsloženije rukovodeće poslove u pojedinim službama obavljaju:
                </p>
                <ul style="margin-left: 40px">
                    <li>Šef službe proizvodnje, distribucije, snabdevanja i razvoja tehničkog sistema</li>
                    <li>Šef računovodstveno finansijske službe</li>
                    <li>Sekretar službe opštih poslova</li>
                </ul>
                <p>
                    Rukovodilac-koordinator i šefovi službi organizuju, objedinjuju i usmeravaju rad pojedinih službi, odnosno zaposlenih u njima, staraju se za blagovremeno, zakonito i pravilno obavljanje poslova iz delokruga službi kojima rukovode.Rukovodilac-koordinator i šefovi službi organizuju, objedinjuju i usmeravaju rad pojedinih službi, odnosno zaposlenih u njima, staraju se za blagovremeno, zakonito i pravilno obavljanje poslova iz delokruga službi kojima rukovode.                </p>
            </div>
        </div>
    </div>
    </section>

    <!-- footer -->
@include('partials/footer')
<!-- footer end here -->

</div>
<!-- main-container end here -->

<!-- javascript -->
<script src="js/jquery.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.smartmenus.min.js"></script>
<script src="js/jquery.scrollUp.js"></script>
<script src="js/jquery.retina.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/icheck.js"></script>

<!-- javascript plugin -->
<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/jquery.player.js"></script>

<!-- javascript core -->
<script src="js/theme-script.js"></script>
<script src="js/twitter/jquery.tweet.js"></script>


<script type="text/javascript">
    jQuery(document).ready(function($){
        // revolution slider configuration here
        $('.slideshow').revolution({
            delay:8000,
            startwidth:1100,
            startheight:300,
            hideThumbs:1,
            navigationType:"none",                  // bullet, thumb, none
            navigationArrows:"solo",                // nexttobullets, solo (old name verticalcentered), none
            navigationStyle:"square",               // round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
            navigationHAlign:"center",              // Vertical Align top,center,bottom
            navigationVAlign:"bottom",              // Horizontal Align left,center,right
            navigationHOffset:0,
            navigationVOffset:0,
            soloArrowLeftHalign:"right",
            soloArrowLeftValign:"bottom",
            soloArrowLeftHOffset:0,
            soloArrowLeftVOffset:42,
            soloArrowRightHalign:"right",
            soloArrowRightValign:"bottom",
            soloArrowRightHOffset:0,
            soloArrowRightVOffset:0,
            touchenabled:"on",                      // Enable Swipe Function : on/off
            onHoverStop:"on",                      // Stop Banner Timet at Hover on Slide on/off
            stopAtSlide:-1,                         // Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
            stopAfterLoops:-1,                      // Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
            hideCaptionAtLimit:0,                   // It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
            hideAllCaptionAtLilmit:0,               // Hide all The Captions if Width of Browser is less then this value
            hideSliderAtLimit:0,                    // Hide the whole slider, and stop also functions if Width of Browser is less than this value
            shadow:0,                               // 0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
            fullWidth:"off",                        // Turns On or Off the Fullwidth Image Centering in FullWidth Modus
            fullScreen:"off",

            parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
        });
    });
</script>
</body>
</html>
