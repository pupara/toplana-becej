<table class="table table-responsive" id="nabavkas-table">
    <thead>
        <th>Naziv</th>
        <th>Dokument</th>
        <th>Sekcija</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($nabavkas as $nabavka)
        <tr>
            <td>{!! $nabavka->naziv !!}</td>
            <td>{!! $nabavka->dokument !!}</td>
            <td>{!! $nabavka->arhiva !!}</td>
            <td>
                {!! Form::open(['route' => ['nabavkas.destroy', $nabavka->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('nabavkas.show', [$nabavka->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('nabavkas.edit', [$nabavka->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>