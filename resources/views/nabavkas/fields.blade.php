<!-- Naziv Field -->
<div class="form-group col-sm-6">
    {!! Form::label('naziv', 'Naziv:') !!}
    {!! Form::text('naziv', null, ['class' => 'form-control']) !!}
</div>

<!-- Dokument Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dokument', 'Dokument:') !!}
    {!! Form::file('dokument') !!}
</div>
<div class="clearfix"></div>

<!-- Arhiva Field -->
<div class="form-group col-sm-6">
    {!! Form::label('arhiva', 'Sekcija:') !!}
        {!! Form::select('arhiva', array('nabavka' => 'Nabavka', 'odluka' => 'Odluka','izvestaj' => 'Finansijski izvestaj'), null, array('class' => 'form-control')) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('nabavkas.index') !!}" class="btn btn-default">Cancel</a>
</div>
