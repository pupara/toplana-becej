@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Nabavka
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($nabavka, ['route' => ['nabavkas.update', $nabavka->id], 'method' => 'patch', 'files'=>'true']) !!}

                        @include('nabavkas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection