<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $nabavka->id !!}</p>
</div>

<!-- Naziv Field -->
<div class="form-group">
    {!! Form::label('naziv', 'Naziv:') !!}
    <p>{!! $nabavka->naziv !!}</p>
</div>

<!-- Dokument Field -->
<div class="form-group">
    {!! Form::label('dokument', 'Dokument:') !!}
    <p>{!! $nabavka->dokument !!}</p>
</div>

<!-- Arhiva Field -->
<div class="form-group">
    {!! Form::label('arhiva', 'Sekcija:') !!}
    <p>{!! $nabavka->arhiva !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $nabavka->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $nabavka->updated_at !!}</p>
</div>

