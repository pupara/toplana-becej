<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="Toplana Bečej" />
    <meta name="description" content="Toplana Bečej">
    <meta name="author" content="www.fuzzy-panda.com">

    <title>Toplana Bečej</title>

    <!-- Retina Bookmark Icon -->
    <link rel="apple-touch-icon-precomposed" href="apple-icon.png" />

    <!-- Reset CSS -->
    <link href="{{asset('css/reset.css')}}" rel="stylesheet" />

    <!-- CSS -->
    <link href="{{asset('css/whhg.css')}}" rel="stylesheet" />
    <link href="{{asset('css/foundstrap.css')}}" rel="stylesheet" />

    <!-- CSS Plugin -->
    <link href="{{asset('css/mediaplayer.css')}}" rel="stylesheet" />
    <link href="{{asset('js/rs-plugin/css/settings.css')}}" rel="stylesheet" media="screen" />

    <!-- epicon stylesheet -->

    <link href="{{asset('css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('css/theme/blue.css')}}" rel="stylesheet" />

    <link href="{{asset('css/epicon-responsive.css')}}" rel="stylesheet" />

    <!-- Theme Option -->
    <link href="{{asset('css/theme-switcher.css')}}" rel="stylesheet" />
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" />


    <script src="{{asset('js/modernizr.js')}}"></script>

</head>
<body style="background: url({{asset('css/theme/pattern/pattern4.png')}}) repeat;">

<!-- main-container -->
<div id="main-container" class="box azure">

    <!-- headaer -->
@include('partials/header')

<!-- header end here -->

    <!-- slider here -->
    <section class="container no-padding" id="slideshow-container">
        <div class="row">
            <div class="inner-container">
                <div class="slideshow">
                    <ul>
                        <!--
                        <li data-transition="fade" data-masterspeed="200">
                            <img src="{{asset('img/cust/becej.png')}}" alt="slider background" style="background:#ffdaba;">

                        <!-- <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="948"
                                 data-y="262"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="1500"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="{{asset('img/cust/toplana.png')}}" alt="image slideshow">
                            </div>

                           <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="653"
                                 data-y="112"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="2000"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-book.png" alt="image slideshow">
                            </div>

                            <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="457"
                                 data-y="59"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="2500"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-notebook.png" alt="image slideshow">
                            </div>

                            <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="533"
                                 data-y="314"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="3000"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-vcard1.png" alt="image slideshow">
                            </div>

                            <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="707"
                                 data-y="413"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="3500"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-vcard2.png" alt="image slideshow">
                            </div>

                            <div class="tp-caption lft ltt slider1-caption2"
                                 data-x="74"
                                 data-y="107"
                                 data-speed="900"
                                 data-endspeed="900"


                                 data-easing="easeOutExpo"
                                 data-endeasing="easeInExpo">
                                <h1 class="white stroke">Web prezentacija</h1>
                            </div>

                            <div class="tp-caption lfb ltb slider1-caption2"
                                 data-x="74"
                                 data-y="167"
                                 data-speed="900"
                                 data-endspeed="900"
                                 data-start="1000"

                                 data-easing="easeOutExpo"
                                 data-endeasing="easeInExpo">
                                <h1 class="white stroke">JKP Toplana Bečej</h1>
                            </div>

                            <div class="tp-caption lfb ltb slider1-caption4"
                                 data-x="74"
                                 data-y="363"
                                 data-speed="900"
                                 data-endspeed="900"
                                 data-start="1500"
                                 data-easing="easeOutExpo"
                                 data-endeasing="easeInExpo">
                                <a class="button-slide1 " href="#">Više o toplani <i class="icon-circleright"></i></a>
                            </div>
                        </li>
                        <!-- slide 1 -->
                        <li data-transition="fade" data-masterspeed="200">
                            <img src="{{asset('img/toplana.JPG')}}" alt="slider background" style="background:#ffdaba;">

                        <!-- <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="948"
                                 data-y="262"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="1500"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="{{asset('img/cust/toplana.png')}}" alt="image slideshow">
                            </div>

                           <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="653"
                                 data-y="112"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="2000"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-book.png" alt="image slideshow">
                            </div>

                            <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="457"
                                 data-y="59"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="2500"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-notebook.png" alt="image slideshow">
                            </div>

                            <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="533"
                                 data-y="314"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="3000"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-vcard1.png" alt="image slideshow">
                            </div>

                            <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="707"
                                 data-y="413"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="3500"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-vcard2.png" alt="image slideshow">
                            </div>

                            <!-- heading caption slider 1 -->
                            <div class="tp-caption lft ltt slider1-caption2"
                                 data-x="74"
                                 data-y="107"
                                 data-speed="900"
                                 data-endspeed="900"


                                 data-easing="easeOutExpo"
                                 data-endeasing="easeInExpo">
                                <h1 class="white stroke"></h1>
                            </div>

                            <div class="tp-caption lfb ltb slider1-caption2"
                                 data-x="74"
                                 data-y="167"
                                 data-speed="300"
                                 data-endspeed="300"
                                 data-start="300"

                                 data-easing="easeOutExpo"
                                 data-endeasing="easeInExpo">
                                <h1 class="white stroke"></h1>
                            </div>

                            <div class="tp-caption lfb ltb slider1-caption4"
                                 data-x="74"
                                 data-y="363"
                                 data-speed="300"
                                 data-endspeed="300"
                                 data-start="600"
                                 data-easing="easeOutExpo"
                                 data-endeasing="easeInExpo">
                                <a class="button-slide1 " href="#">Više o toplani <i class="icon-circleright"></i></a>
                            </div>
                        </li>
                        <li data-transition="fade" data-masterspeed="200">
                            <img src="{{asset('img/pogon.JPG')}}" alt="slider background" style="background:#ffdaba;">

                        <!-- <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="948"
                                 data-y="262"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="1500"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="{{asset('img/cust/toplana.png')}}" alt="image slideshow">
                            </div>

                           <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="653"
                                 data-y="112"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="2000"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-book.png" alt="image slideshow">
                            </div>

                            <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="457"
                                 data-y="59"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="2500"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-notebook.png" alt="image slideshow">
                            </div>

                            <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="533"
                                 data-y="314"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="3000"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-vcard1.png" alt="image slideshow">
                            </div>

                            <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="707"
                                 data-y="413"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="3500"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-vcard2.png" alt="image slideshow">
                            </div>

                            <!-- heading caption slider 1 -->
                            <div class="tp-caption lft ltt slider1-caption2"
                                 data-x="74"
                                 data-y="107"
                                 data-speed="900"
                                 data-endspeed="900"


                                 data-easing="easeOutExpo"
                                 data-endeasing="easeInExpo">
                                <h1 class="white stroke"></h1>
                            </div>

                            <div class="tp-caption lfb ltb slider1-caption2"
                                 data-x="74"
                                 data-y="167"
                                 data-speed="300"
                                 data-endspeed="300"
                                 data-start="300"

                                 data-easing="easeOutExpo"
                                 data-endeasing="easeInExpo">
                                <h1 class="white stroke"></h1>
                            </div>

                            <div class="tp-caption lfb ltb slider1-caption4"
                                 data-x="74"
                                 data-y="363"
                                 data-speed="300"
                                 data-endspeed="300"
                                 data-start="600"
                                 data-easing="easeOutExpo"
                                 data-endeasing="easeInExpo">
                                <a class="button-slide1 " href="#">Više o toplani <i class="icon-circleright"></i></a>
                            </div>
                        </li>
                        <li data-transition="fade" data-masterspeed="200">
                                <img src="{{asset('img/SCADA.JPG')}}" alt="slider background" style="background:#ffdaba;">

                        <!-- <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="948"
                                 data-y="262"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="1500"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="{{asset('img/cust/toplana.png')}}" alt="image slideshow">
                            </div>

                           <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="653"
                                 data-y="112"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="2000"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-book.png" alt="image slideshow">
                            </div>

                            <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="457"
                                 data-y="59"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="2500"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-notebook.png" alt="image slideshow">
                            </div>

                            <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="533"
                                 data-y="314"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="3000"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-vcard1.png" alt="image slideshow">
                            </div>

                            <div class="tp-caption sft stt rs-parallaxlevel-4"
                                 data-x="707"
                                 data-y="413"
                                 data-speed="4000"
                                 data-endspeed="1000"
                                 data-start="3500"
                                 data-end="8500"
                                 data-easing="easeOutElastic"
                                 data-endeasing="easeInExpo">
                                <img src="img/slideshow/slide1-vcard2.png" alt="image slideshow">
                            </div>

                            <!-- heading caption slider 1 -->
                            <div class="tp-caption lft ltt slider1-caption2"
                                 data-x="74"
                                 data-y="107"
                                 data-speed="900"
                                 data-endspeed="900"


                                 data-easing="easeOutExpo"
                                 data-endeasing="easeInExpo">
                                <h1 class="white stroke"></h1>
                            </div>

                            <div class="tp-caption lfb ltb slider1-caption2"
                                 data-x="74"
                                 data-y="167"
                                 data-speed="300"
                                 data-endspeed="300"
                                 data-start="300"

                                 data-easing="easeOutExpo"
                                 data-endeasing="easeInExpo">
                                <h1 class="white stroke"></h1>
                            </div>

                            <div class="tp-caption lfb ltb slider1-caption4"
                                 data-x="74"
                                 data-y="363"
                                 data-speed="300"
                                 data-endspeed="300"
                                 data-start="600"
                                 data-easing="easeOutExpo"
                                 data-endeasing="easeInExpo">
                                <a class="button-slide1 " href="#">Više o toplani <i class="icon-circleright"></i></a>
                            </div>
                        </li>

                        <!-- slide 2 -->


                        <!-- slide 3 -->

                        <!-- slide 4 -->

                    </ul>

                    <div class="tp-bannertimer"></div>

                </div>
            </div>
        </div>
    </section>
    <!-- slider end here -->


    <section class="container no-padding">
        <div class="row">
            <div class="inner-container epic-animate" data-animate="fadeInDown" id="content-home-bg1">
                <div class="large-10 large-push-1 column text-center">
                    <div class="heading-title bottom-line">
                        <h3 class="title">Dodbrodošli na web prezentaciju <br><strong class="epicon-strong">JP Toplane Bečej</strong></h3>

                    </div>

                    <p>
                        Na internet prezentaciji se možete informisati o najnovijim vestima, obaveštenjima, informacijama vezanim za korisnike. Takođe na našim stranama su istaknute informacije o grejnoj sezoni, finansijskim izveštajima i aktuelnim tenderima.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="container no-padding">
        <div class="row">

            <div class="inner-container "  id="features">
                <div class="large-4 medium-4 medium-potrait-12 small-12 column">
                    <div class="box-icon left small">
                        <div class="icon-container">
                            <div class="icon-shape circle">
                                <i class="icon-alertalt"></i>
                            </div>
                        </div>
                        <div class="text-container">
                            <a href="{{route('vesti')}}"> <h4 class="title-box"><strong class="epicon-strong">Vesti</strong></h4></a>
                            <p style="padding-bottom: 26px;">

                                Više informacija o najnovijim vestima vezanim za rad toplane.
                            </p>
                        </div>
                    </div>

                </div>

                <div class="large-4 medium-4 medium-potrait-12 small-12 column">
                    <div class="box-icon left small">
                        <div class="icon-container">
                            <div class="icon-shape circle">
                                <i class="icon-lightning"></i>
                            </div>
                        </div>
                        <div class="text-container">
                            <a href="{{route('korisnicki-servis')}}#grejnasezona"> <h4 class="title-box"><strong class="epicon-strong">Grejna sezona</strong></h4></a>
                            <p>
                                Više informacija o korisničkom servisu - informacije o ceni toplotne energije, očitavanju brojila i grejnos sezoni.
                            </p>
                        </div>
                    </div>

                </div>

                <div class="large-4 medium-4 medium-potrait-12 small-12 column">
                    <div class="box-icon left small">
                        <div class="icon-container">
                            <div class="icon-shape circle">
                                <i class="icon-align-justify"></i>
                            </div>
                        </div>
                        <div class="text-container">
                            <a href="{{route('aktuelnosti')}}">  <h4 class="title-box"><strong class="epicon-strong">Aktuelnosti</strong></h4></a>
                            <p style="padding-bottom: 26px;">
                                Više informacija o najnovijim aktuelnostima vezanim za rad toplane.
                            </p>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>
    <section class="container no-padding-top azure">
        <div class="row">
            <div class="large-12 column " data-animate="fadeInDown">
                <div class="promo-box">
                    <div class="promo-text">
                        <h4><strong class="epicon-strong red2">  <i class="icon-callalt"></i>  TELEFON ZA HITNE INTERVENCIJE </strong> <span style="margin-left: 40px">021 6915 470</span></h4>
                    </div>
                    <div class="promo-action">
                        <a class="button button-group" href="{{route('kontakt')}}">Kontaktirajte nas
                            <span><i class="icon-chevron-right"></i></span>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="container azure">
        <div class="row epic-animate" data-animate="fadeInDown">
            <div class="large-8 medium-8 medium-potrait-6 column">
               <h3 class="inline-heading-line"><span> Aktuelnosti</span></h3>
                <ul class="no-bullet event-list no-margin">
                    <li class="aktuelnosti-margin">
                        <div class="panel aktuelnosti">
                            <h5 class="h5-margin"><strong class="epicon-strong"><a href="{!! route('aktuelnostiSlug', [$aktuelnost->slug]) !!}">{!! $aktuelnost->naslov !!}</a></strong></h5><br>
                            <p>
                                {!! str_limit( strip_tags($aktuelnost->tekst), 770)!!}

                            </p>
                            <a class="" href="{!! route('aktuelnostiSlug', [$aktuelnost->slug]) !!}">Više detalja
                                <span><i class="icon-chevron-right"></i></span>
                            </a>
                        </div>
                    </li>

                </ul>
            </div>



            <div class="large-4 medium-4 medium-potrait-12 column">
                <h3 class="inline-heading-line"><span>Informacije</span></h3>
                <ul class="no-bullet hotelinfo-list no-margin">
                    <li><i class="icon-factory"></i><a href="{{route('istorijat')}}#istorijat"><strong>O preduzeću</strong></a></li>
                    <li><i class="icon-stickynote"></i><a href="{{route('javne-nabavke')}}"><strong>Javne nabavke</strong></a></li>
                    <li><i class="icon-report"></i><a href="{{route('finansijski-izvestaji')}}"><strong>Finansijski izveštaji</strong></a></li>
                    <li><i class="icon-wavealt-seaalt"></i><a href="{{route('korisnicki-servis')}}#grejnasezona"><strong>Grejna sezona</strong></a></li>
                    <li><i class="icon-envelope"></i><a href="{{route('kontakt')}}"><strong>Kontakt</strong></a></li>
                </ul>
            </div>
        </div>
    </section>
    @if($blogs)
    <section class="container azure">
        <div class="row news-background">
            <div class="large-3 medium-3 medium-potrait-12 small-12 column epic-animate " data-animate="fadeInDown">
                <div class="blog-slider-nav-container">
                    <div class="heading-title bottom-line news-top">
                        <h3 class="title"><strong class="epicon-strong">Najnovije</strong> vesti</h3>
                    </div>


                    <div class="blog-slider-nav">
                        <div class="left-nav">
                            <i class="icon-chevron-left"></i>
                        </div>
                        <div class="right-nav">
                            <i class="icon-chevron-right"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="large-9 medium-9 medium-potrait-12 small-12 column news-top news-bottom">
                <div class="blog-slider-container epic-animate" data-animate="fadeInDown">

                    @foreach($blogs as $blog)
                    <div class="blog-post-item">
                        <a class="blog-post-title" href="{!! route('vestiSlug', [$blog->slug]) !!}">
                            <h4>{!! $blog->naslov !!}</h4>
                        </a>
                        <ul class="blog-post-info no-bullet clearfix">
                            <li><i class="icon-clockalt-timealt"></i>{{ Carbon\Carbon::parse($blog->created_at)->format('d.m.Y') }}</li>
                            <li></i></li>
                        </ul>
                        <p>
                            {{ str_limit( strip_tags($blog->tekst), 200)}}
                        </p>
                        <a class="" href="{!! route('vestiSlug', [$blog->slug]) !!}">Više detalja
                            <span><i class="icon-chevron-right"></i></span>
                        </a>
                    </div>
                    @endforeach


                </div>
            </div>
        </div>
    </section>

@endif

    <!-- footer -->
@include('partials/footer')
<!-- footer end here -->

</div>
<!-- main-container end here -->

<!-- javascript -->
<script src="js/jquery.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.smartmenus.min.js"></script>
<script src="js/jquery.scrollUp.js"></script>
<script src="js/jquery.retina.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/icheck.js"></script>

<!-- javascript plugin -->
<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/jquery.player.js"></script>

<!-- javascript core -->
<script src="js/theme-script.js"></script>
<script src="js/twitter/jquery.tweet.js"></script>


<script type="text/javascript">
    jQuery(document).ready(function($){
        // revolution slider configuration here
        $('.slideshow').revolution({
            delay:8000,
            startwidth:1100,
            startheight:600,
            hideThumbs:1,
            navigationType:"none",                  // bullet, thumb, none
            navigationArrows:"solo",                // nexttobullets, solo (old name verticalcentered), none
            navigationStyle:"square",               // round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
            navigationHAlign:"center",              // Vertical Align top,center,bottom
            navigationVAlign:"bottom",              // Horizontal Align left,center,right
            navigationHOffset:0,
            navigationVOffset:0,
            soloArrowLeftHalign:"right",
            soloArrowLeftValign:"bottom",
            soloArrowLeftHOffset:0,
            soloArrowLeftVOffset:42,
            soloArrowRightHalign:"right",
            soloArrowRightValign:"bottom",
            soloArrowRightHOffset:0,
            soloArrowRightVOffset:0,
            touchenabled:"on",                      // Enable Swipe Function : on/off
            onHoverStop:"on",                      // Stop Banner Timet at Hover on Slide on/off
            stopAtSlide:-1,                         // Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
            stopAfterLoops:-1,                      // Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
            hideCaptionAtLimit:0,                   // It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
            hideAllCaptionAtLilmit:0,               // Hide all The Captions if Width of Browser is less then this value
            hideSliderAtLimit:0,                    // Hide the whole slider, and stop also functions if Width of Browser is less than this value
            shadow:0,                               // 0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
            fullWidth:"off",                        // Turns On or Off the Fullwidth Image Centering in FullWidth Modus
            fullScreen:"off",

            parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
        });
    });
</script>
</body>
</html>
