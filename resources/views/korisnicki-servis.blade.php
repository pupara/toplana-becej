<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="Toplana Bečej" />
    <meta name="description" content="Toplana Bečej">
    <meta name="author" content="www.fuzzy-panda.com">

    <title>Toplana Bečej</title>

    <!-- Retina Bookmark Icon -->
    <link rel="apple-touch-icon-precomposed" href="apple-icon.png" />

    <!-- Reset CSS -->
    <link href="{{asset('css/reset.css')}}" rel="stylesheet" />

    <!-- CSS -->
    <link href="{{asset('css/whhg.css')}}" rel="stylesheet" />
    <link href="{{asset('css/foundstrap.css')}}" rel="stylesheet" />

    <!-- CSS Plugin -->
    <link href="{{asset('css/mediaplayer.css')}}" rel="stylesheet" />
    <link href="{{asset('js/rs-plugin/css/settings.css')}}" rel="stylesheet" media="screen" />

    <!-- epicon stylesheet -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('css/epicon-responsive.css')}}" rel="stylesheet" />

    <!-- Theme Option -->
    <link href="{{asset('css/theme-switcher.css')}}" rel="stylesheet" />
    <link href="{{asset('css/theme/blue.css')}}" rel="stylesheet" />
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" />


    <script src="{{asset('js/modernizr.js')}}"></script>

</head>
<body style="background: url({{asset('css/theme/pattern/pattern4.png')}}) repeat;">

<!-- main-container -->
<div id="main-container" class="box">

    <!-- headaer -->
@include('partials/header')

<!-- header end here -->

    <!-- slider here -->
    <section class="container" id="page-header">
        <div class="row">
            <div class="inner-container">
                <div class="large-12 column text-right epic-animate" data-animate="fadeInDown">
                    <h1>Korisnički servis<span class="epicon-color"></span></h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Korisnički servis</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- slider end here -->


    <section class="container">
        <div class="row">
            <div class="large-12 medium-12 column epic-animate" data-animate="fadeInDown">

                <div class="panel border-no" id="grejnasezona">
                    <h3 class="gap" data-gap-top="0"><strong class="epicon-strong red">CENA TOPLOTNE ENERGIJE</strong></h3>
                    @if( ! empty($cena->naslov))
                        <h5>{!! $cena->naslov!!}}</h5>
                    @endif
                    {!! $cena->tekst !!}
                </div>
                <div class="panel border-no" id="ocitavanjebrojila">
                    <h3 class="gap" data-gap-top="0"><strong class="epicon-strong red">OČITAVANJE MERAČA</strong></h3>
                    @if( ! empty($brojilo->naslov))
                        <p>{!! $brojilo->naslov!!}</p>
                    @endif
                    @if( ! empty($brojilo->tekst))
                        <p>{!! $brojilo->tekst!!}</p>
                    @endif
                    @if( ! empty($brojilo->slika))
                        <img src="{{ asset($brojilo->slika) }}"></img>
                    @endif
                </div>
                <div class="panel border-no" id="grejnasezona">
                    <h3 class="gap" data-gap-top="0"><strong class="epicon-strong red">GREJNA SEZONA</strong></h3>

                    <p>
                        {!! $grejna->tekst!!}
                    </p>

                </div>
            </div>
        </div>
    </section>

    <!-- footer -->
@include('partials/footer')
<!-- footer end here -->

</div>
<!-- main-container end here -->

<!-- javascript -->
<script src="js/jquery.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.smartmenus.min.js"></script>
<script src="js/jquery.scrollUp.js"></script>
<script src="js/jquery.retina.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/icheck.js"></script>

<!-- javascript plugin -->
<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/jquery.player.js"></script>

<!-- javascript core -->
<script src="js/theme-script.js"></script>
<script src="js/twitter/jquery.tweet.js"></script>


<script type="text/javascript">
    jQuery(document).ready(function($){
        // revolution slider configuration here
        $('.slideshow').revolution({
            delay:8000,
            startwidth:1100,
            startheight:300,
            hideThumbs:1,
            navigationType:"none",                  // bullet, thumb, none
            navigationArrows:"solo",                // nexttobullets, solo (old name verticalcentered), none
            navigationStyle:"square",               // round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
            navigationHAlign:"center",              // Vertical Align top,center,bottom
            navigationVAlign:"bottom",              // Horizontal Align left,center,right
            navigationHOffset:0,
            navigationVOffset:0,
            soloArrowLeftHalign:"right",
            soloArrowLeftValign:"bottom",
            soloArrowLeftHOffset:0,
            soloArrowLeftVOffset:42,
            soloArrowRightHalign:"right",
            soloArrowRightValign:"bottom",
            soloArrowRightHOffset:0,
            soloArrowRightVOffset:0,
            touchenabled:"on",                      // Enable Swipe Function : on/off
            onHoverStop:"on",                      // Stop Banner Timet at Hover on Slide on/off
            stopAtSlide:-1,                         // Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
            stopAfterLoops:-1,                      // Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
            hideCaptionAtLimit:0,                   // It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
            hideAllCaptionAtLilmit:0,               // Hide all The Captions if Width of Browser is less then this value
            hideSliderAtLimit:0,                    // Hide the whole slider, and stop also functions if Width of Browser is less than this value
            shadow:0,                               // 0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
            fullWidth:"off",                        // Turns On or Off the Fullwidth Image Centering in FullWidth Modus
            fullScreen:"off",

            parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
        });
    });
</script>
</body>
</html>
