<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="Toplana Bečej" />
    <meta name="description" content="Toplana Bečej">
    <meta name="author" content="www.fuzzy-panda.com">

    <title>Toplana Bečej</title>

    <!-- Retina Bookmark Icon -->
    <link rel="apple-touch-icon-precomposed" href="apple-icon.png" />

    <!-- Reset CSS -->
    <link href="{{asset('css/reset.css')}}" rel="stylesheet" />

    <!-- CSS -->
    <link href="{{asset('css/whhg.css')}}" rel="stylesheet" />
    <link href="{{asset('css/foundstrap.css')}}" rel="stylesheet" />

    <!-- CSS Plugin -->
    <link href="{{asset('css/mediaplayer.css')}}" rel="stylesheet" />
    <link href="{{asset('js/rs-plugin/css/settings.css')}}" rel="stylesheet" media="screen" />

    <!-- epicon stylesheet -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet" />
    <link href="{{asset('css/epicon-responsive.css')}}" rel="stylesheet" />

    <!-- Theme Option -->
    <link href="{{asset('css/theme-switcher.css')}}" rel="stylesheet" />
    <link href="{{asset('css/theme/blue.css')}}" rel="stylesheet" />
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" />


    <script src="{{asset('js/modernizr.js')}}"></script>

</head>
<body style="background: url({{asset('css/theme/pattern/pattern4.png')}}) repeat;">

<!-- main-container -->
<div id="main-container" class="box">

    <!-- headaer -->
@include('partials/header')

<!-- header end here -->

    <!-- slider here -->
    <section class="container" id="page-header">
        <div class="row">
            <div class="inner-container">
                <div class="large-12 column text-right epic-animate" data-animate="fadeInDown">
                    <h1>Tehnički podaci</h1>
                    <ol class="breadcrumb">
                        <li><a href="#">O nama</a></li>
                        <li class="">Tehnički podaci</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- slider end here -->


    <section class="container">
        <div class="row">
            <div class="large-12 medium-12 column epic-animate" data-animate="fadeInDown">
                <p>
                   <strong> <span class="dropcap red">S</span>
                       Sistem daljinskog grejanja u Bečeju čine toplotni izvor, koji je lociran u ulici Petrovoselski put br.3, distributivna mreža, koja je dvocevna i čine je magistralni vrelovod u obliku prstena, primarni i priključni vodovi i toplotne podstanice korisnika. Projektovani temperaturski režim je 130/75°C, a nazivni pritisak 16 bara. Instalisani kapacitet toplotnog izvora je 18,6 MW, kojim se upravlja centralno preko nadzorno-upravljačkog sistema. Količina isporučene toplotne energije iz toplotnog izvora reguliše se centralno u zavisnosti od temperaturi spoljašnjeg vazduha putem centralne kvalitativne regulacija.                   </strong>
                </p>
                <div class="panel border-no">
                <h3 class="gap" data-gap-top="0"><strong class="epicon-strong red">TOPLOTNI IZVOR</strong></h3>
                <p>
                    Toplotni izvor čine dva vrelovodna kotla tipa VKLM-8, pojedinačne toplotne snage 9,3 MW. Kotlovi su izrađeni u membranskoj izvedbi, a proizvedeni su u pogonima TPK Zagreb 1985. godine. Opremljeni su gorionicima Saacke koji su predviđeni za rad na prirodni gas.
                <p>
                        Cirkulacija grejnog medija-voda u distributivnoj mreži vrši se preko cirkulacionog postrojenja koje se sastoji od 4 GRUNDFOS cirkulacione pumpe sa ugrađenim DANFOSS frekventnim regulatorima.                </p>
                <p>
                    U postrojenju za hemijsku pripremu i kondicioniranje vode vrši se prvenstveno omekšavanje vodovodske vode koja se putem pumpi za dopunu i održavanje statičkog pritiska ubacuje u distributivni sistem. Održavanje pritiska u distributivnom sistemu vrši se na povratu.                </p>
                <p>
                    Upravljanje toplotnim izvorom vrši se putem centralnog nadzorno-upravljačkog

                    sistema.
                </p>
                </div>
                <div class="panel border-no">
                <h3 class="gap" data-gap-top="0"><strong class="epicon-strong red">DISTRIBUTIVNA MREŽA</strong></h3>
                <p>
                    Distributivna mreža obuhvata sve cevovode od toplotnog izvora pa do ulaznih ventila u priključnim podstanicama. Sastoji se iz magistralnog vrelovoda, primarnih i priključnih vrelovoda. Magistralni vrelovod je izrađen u obliku prstena, gde je unutrašnja cev potisna, a spoljašnja povratna. Radi poboljšanja hidrauličkih karakteristika distributivne mreže u toku 2016. godine izgrađena je kratka veza na prstenu spojanjem dve tačke magistralnog vrelovoda koje se nalaze na severnoj odnosno južnoj strani prstena.                </p>
                <p>
                    Kapacitet distributivne mreže je 60MW. Ukupna dužina distributivne mreže je 23 kmTr, a ekvivalentni prečnik je DN80, pri čemu su zastupljeni prečnici od DN 25 do DN 300 sa kojim vrelovod izlazi iz Toplane. Magistralni vrelovod je dimenzije DN250 sa kratkom vezom DN150.                </p>
                <p>
                    Vrelovodna mreža je dvocevna i u potpunosti je izrađena od predizolovanih cevi. Najvećim delom cevi su položene beskanalno podzemno, dok je nadzemno polaganje zastupljeno samo u slučajevima gde podzemno polaganje nije bilo moguće.                </p>
                </div>
                <div class="panel border-no">
                <h3 class="gap" data-gap-top="0"><strong class="epicon-strong red">TOPLOTNE PODSTANICE</strong></h3>

                <p>
                    Ukupan instalisani kapacitet toplotnih podstanica iznosi cca 20 MW, i u sistemu ih ima oko 410. Oko 80 podstanica pripada kolektivnim stambenim objektima, ustanovama i poslovnim korisnicima, dok ostale pripadaju individualnim stambenim objektima.                </p>
                <p>
                    Sve toplotne podstanice u sistemu su indirektnog tipa (prenos toplote između primar i sekundara vrši se putem izmenjivača toplote) čiji je projektovani režim rada 130/75ºC na primaru, odnosno 90/70ºC na sekundaru.                </p>
                    <p>
                        U četiri toplotne podstanice preko kojih se toplotnom energijom snabdevaju višestambeni objekti ugrađena je oprema za daljinski nadzor i upravljanje.                    </p>
                <p>
                    U toplotne podstanice su trenutno ugradjene dve vrste izmenjivača:
                    <ul style="margin-left: 40px">
                        <li>dobošasti sa spiralnim bakarnim cevima</li>
                        <li>pločasti (lemljeni)</li>
                    </ul>
                </p>
                <p>
                    Dotrajali i neefikasni dobošasti izmenjivači toplote se intezivno zamenjuju sa pločastim izmenjivačima toplote.
                </p>
                <p>
                    Kod podstanica malih kapaciteta koje pripadaju uglavnom individualnim korisnicima regulacija protoka se vrši ručnim regulacionim ventilima, a u podstanicama većih kapaciteta predviđena je dinamička regulacija protoka prolaznim elektromotornim ventilima sa trotačkastom regulacijom.                </p>
                <p>
                    Sve toplotne podstanice imaju ugrađene merače toplotne energije tako da se naplata isporučene toplotne energije vrši isključivo prema izmerenim količinama toplotne energije.
                </p>
                <p>
                    Počevši sa grejnom sezonom 2012/2013 u 32 višestambena objekta obračun isporučene toplotne energije po stambenim jedinicama u objektu vrši se preko delitelja troškova.


                </p>
                </div>
            </div>
        </div>
    </section>

    <!-- footer -->
@include('partials/footer')
<!-- footer end here -->

</div>
<!-- main-container end here -->

<!-- javascript -->
<script src="js/jquery.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.smartmenus.min.js"></script>
<script src="js/jquery.scrollUp.js"></script>
<script src="js/jquery.retina.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/icheck.js"></script>

<!-- javascript plugin -->
<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/jquery.player.js"></script>

<!-- javascript core -->
<script src="js/theme-script.js"></script>
<script src="js/twitter/jquery.tweet.js"></script>


<script type="text/javascript">
    jQuery(document).ready(function($){
        // revolution slider configuration here
        $('.slideshow').revolution({
            delay:8000,
            startwidth:1100,
            startheight:300,
            hideThumbs:1,
            navigationType:"none",                  // bullet, thumb, none
            navigationArrows:"solo",                // nexttobullets, solo (old name verticalcentered), none
            navigationStyle:"square",               // round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
            navigationHAlign:"center",              // Vertical Align top,center,bottom
            navigationVAlign:"bottom",              // Horizontal Align left,center,right
            navigationHOffset:0,
            navigationVOffset:0,
            soloArrowLeftHalign:"right",
            soloArrowLeftValign:"bottom",
            soloArrowLeftHOffset:0,
            soloArrowLeftVOffset:42,
            soloArrowRightHalign:"right",
            soloArrowRightValign:"bottom",
            soloArrowRightHOffset:0,
            soloArrowRightVOffset:0,
            touchenabled:"on",                      // Enable Swipe Function : on/off
            onHoverStop:"on",                      // Stop Banner Timet at Hover on Slide on/off
            stopAtSlide:-1,                         // Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
            stopAfterLoops:-1,                      // Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
            hideCaptionAtLimit:0,                   // It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
            hideAllCaptionAtLilmit:0,               // Hide all The Captions if Width of Browser is less then this value
            hideSliderAtLimit:0,                    // Hide the whole slider, and stop also functions if Width of Browser is less than this value
            shadow:0,                               // 0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
            fullWidth:"off",                        // Turns On or Off the Fullwidth Image Centering in FullWidth Modus
            fullScreen:"off",

            parallax:"mouse",
            parallaxBgFreeze:"on",
            parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
        });
    });
</script>
</body>
</html>