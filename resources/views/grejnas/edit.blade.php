@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Grejna sezona
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($grejna, ['route' => ['grejnas.update', $grejna->id], 'method' => 'patch']) !!}

                        @include('grejnas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection