<table class="table table-responsive" id="grejnas-table">
    <thead>
        <th>Tekst</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($grejnas as $grejna)
        <tr>
            <td>{!! $grejna->tekst !!}</td>
            <td>
                {!! Form::open(['route' => ['grejnas.destroy', $grejna->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('grejnas.show', [$grejna->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('grejnas.edit', [$grejna->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>