<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $grejna->id !!}</p>
</div>

<!-- Tekst Field -->
<div class="form-group">
    {!! Form::label('tekst', 'Tekst:') !!}
    <p>{!! $grejna->tekst !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $grejna->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $grejna->updated_at !!}</p>
</div>

