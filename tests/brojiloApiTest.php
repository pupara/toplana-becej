<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class brojiloApiTest extends TestCase
{
    use MakebrojiloTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatebrojilo()
    {
        $brojilo = $this->fakebrojiloData();
        $this->json('POST', '/api/v1/brojilos', $brojilo);

        $this->assertApiResponse($brojilo);
    }

    /**
     * @test
     */
    public function testReadbrojilo()
    {
        $brojilo = $this->makebrojilo();
        $this->json('GET', '/api/v1/brojilos/'.$brojilo->id);

        $this->assertApiResponse($brojilo->toArray());
    }

    /**
     * @test
     */
    public function testUpdatebrojilo()
    {
        $brojilo = $this->makebrojilo();
        $editedbrojilo = $this->fakebrojiloData();

        $this->json('PUT', '/api/v1/brojilos/'.$brojilo->id, $editedbrojilo);

        $this->assertApiResponse($editedbrojilo);
    }

    /**
     * @test
     */
    public function testDeletebrojilo()
    {
        $brojilo = $this->makebrojilo();
        $this->json('DELETE', '/api/v1/brojilos/'.$brojilo->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/brojilos/'.$brojilo->id);

        $this->assertResponseStatus(404);
    }
}
