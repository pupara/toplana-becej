<?php

use App\Models\grejna;
use App\Repositories\grejnaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class grejnaRepositoryTest extends TestCase
{
    use MakegrejnaTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var grejnaRepository
     */
    protected $grejnaRepo;

    public function setUp()
    {
        parent::setUp();
        $this->grejnaRepo = App::make(grejnaRepository::class);
    }

    /**
     * @test create
     */
    public function testCreategrejna()
    {
        $grejna = $this->fakegrejnaData();
        $createdgrejna = $this->grejnaRepo->create($grejna);
        $createdgrejna = $createdgrejna->toArray();
        $this->assertArrayHasKey('id', $createdgrejna);
        $this->assertNotNull($createdgrejna['id'], 'Created grejna must have id specified');
        $this->assertNotNull(grejna::find($createdgrejna['id']), 'grejna with given id must be in DB');
        $this->assertModelData($grejna, $createdgrejna);
    }

    /**
     * @test read
     */
    public function testReadgrejna()
    {
        $grejna = $this->makegrejna();
        $dbgrejna = $this->grejnaRepo->find($grejna->id);
        $dbgrejna = $dbgrejna->toArray();
        $this->assertModelData($grejna->toArray(), $dbgrejna);
    }

    /**
     * @test update
     */
    public function testUpdategrejna()
    {
        $grejna = $this->makegrejna();
        $fakegrejna = $this->fakegrejnaData();
        $updatedgrejna = $this->grejnaRepo->update($fakegrejna, $grejna->id);
        $this->assertModelData($fakegrejna, $updatedgrejna->toArray());
        $dbgrejna = $this->grejnaRepo->find($grejna->id);
        $this->assertModelData($fakegrejna, $dbgrejna->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletegrejna()
    {
        $grejna = $this->makegrejna();
        $resp = $this->grejnaRepo->delete($grejna->id);
        $this->assertTrue($resp);
        $this->assertNull(grejna::find($grejna->id), 'grejna should not exist in DB');
    }
}
