<?php

use App\Models\aktuelnost;
use App\Repositories\aktuelnostRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class aktuelnostRepositoryTest extends TestCase
{
    use MakeaktuelnostTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var aktuelnostRepository
     */
    protected $aktuelnostRepo;

    public function setUp()
    {
        parent::setUp();
        $this->aktuelnostRepo = App::make(aktuelnostRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateaktuelnost()
    {
        $aktuelnost = $this->fakeaktuelnostData();
        $createdaktuelnost = $this->aktuelnostRepo->create($aktuelnost);
        $createdaktuelnost = $createdaktuelnost->toArray();
        $this->assertArrayHasKey('id', $createdaktuelnost);
        $this->assertNotNull($createdaktuelnost['id'], 'Created aktuelnost must have id specified');
        $this->assertNotNull(aktuelnost::find($createdaktuelnost['id']), 'aktuelnost with given id must be in DB');
        $this->assertModelData($aktuelnost, $createdaktuelnost);
    }

    /**
     * @test read
     */
    public function testReadaktuelnost()
    {
        $aktuelnost = $this->makeaktuelnost();
        $dbaktuelnost = $this->aktuelnostRepo->find($aktuelnost->id);
        $dbaktuelnost = $dbaktuelnost->toArray();
        $this->assertModelData($aktuelnost->toArray(), $dbaktuelnost);
    }

    /**
     * @test update
     */
    public function testUpdateaktuelnost()
    {
        $aktuelnost = $this->makeaktuelnost();
        $fakeaktuelnost = $this->fakeaktuelnostData();
        $updatedaktuelnost = $this->aktuelnostRepo->update($fakeaktuelnost, $aktuelnost->id);
        $this->assertModelData($fakeaktuelnost, $updatedaktuelnost->toArray());
        $dbaktuelnost = $this->aktuelnostRepo->find($aktuelnost->id);
        $this->assertModelData($fakeaktuelnost, $dbaktuelnost->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteaktuelnost()
    {
        $aktuelnost = $this->makeaktuelnost();
        $resp = $this->aktuelnostRepo->delete($aktuelnost->id);
        $this->assertTrue($resp);
        $this->assertNull(aktuelnost::find($aktuelnost->id), 'aktuelnost should not exist in DB');
    }
}
