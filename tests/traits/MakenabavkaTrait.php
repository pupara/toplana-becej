<?php

use Faker\Factory as Faker;
use App\Models\nabavka;
use App\Repositories\nabavkaRepository;

trait MakenabavkaTrait
{
    /**
     * Create fake instance of nabavka and save it in database
     *
     * @param array $nabavkaFields
     * @return nabavka
     */
    public function makenabavka($nabavkaFields = [])
    {
        /** @var nabavkaRepository $nabavkaRepo */
        $nabavkaRepo = App::make(nabavkaRepository::class);
        $theme = $this->fakenabavkaData($nabavkaFields);
        return $nabavkaRepo->create($theme);
    }

    /**
     * Get fake instance of nabavka
     *
     * @param array $nabavkaFields
     * @return nabavka
     */
    public function fakenabavka($nabavkaFields = [])
    {
        return new nabavka($this->fakenabavkaData($nabavkaFields));
    }

    /**
     * Get fake data of nabavka
     *
     * @param array $postFields
     * @return array
     */
    public function fakenabavkaData($nabavkaFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'naziv' => $fake->word,
            'dokument' => $fake->word,
            'arhiva' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $nabavkaFields);
    }
}
