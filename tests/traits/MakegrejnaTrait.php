<?php

use Faker\Factory as Faker;
use App\Models\grejna;
use App\Repositories\grejnaRepository;

trait MakegrejnaTrait
{
    /**
     * Create fake instance of grejna and save it in database
     *
     * @param array $grejnaFields
     * @return grejna
     */
    public function makegrejna($grejnaFields = [])
    {
        /** @var grejnaRepository $grejnaRepo */
        $grejnaRepo = App::make(grejnaRepository::class);
        $theme = $this->fakegrejnaData($grejnaFields);
        return $grejnaRepo->create($theme);
    }

    /**
     * Get fake instance of grejna
     *
     * @param array $grejnaFields
     * @return grejna
     */
    public function fakegrejna($grejnaFields = [])
    {
        return new grejna($this->fakegrejnaData($grejnaFields));
    }

    /**
     * Get fake data of grejna
     *
     * @param array $postFields
     * @return array
     */
    public function fakegrejnaData($grejnaFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'tekst' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $grejnaFields);
    }
}
