<?php

use Faker\Factory as Faker;
use App\Models\aktuelnost;
use App\Repositories\aktuelnostRepository;

trait MakeaktuelnostTrait
{
    /**
     * Create fake instance of aktuelnost and save it in database
     *
     * @param array $aktuelnostFields
     * @return aktuelnost
     */
    public function makeaktuelnost($aktuelnostFields = [])
    {
        /** @var aktuelnostRepository $aktuelnostRepo */
        $aktuelnostRepo = App::make(aktuelnostRepository::class);
        $theme = $this->fakeaktuelnostData($aktuelnostFields);
        return $aktuelnostRepo->create($theme);
    }

    /**
     * Get fake instance of aktuelnost
     *
     * @param array $aktuelnostFields
     * @return aktuelnost
     */
    public function fakeaktuelnost($aktuelnostFields = [])
    {
        return new aktuelnost($this->fakeaktuelnostData($aktuelnostFields));
    }

    /**
     * Get fake data of aktuelnost
     *
     * @param array $postFields
     * @return array
     */
    public function fakeaktuelnostData($aktuelnostFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'naslov' => $fake->text,
            'tekst' => $fake->text,
            'slug' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $aktuelnostFields);
    }
}
