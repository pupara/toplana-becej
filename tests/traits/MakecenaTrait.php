<?php

use Faker\Factory as Faker;
use App\Models\cena;
use App\Repositories\cenaRepository;

trait MakecenaTrait
{
    /**
     * Create fake instance of cena and save it in database
     *
     * @param array $cenaFields
     * @return cena
     */
    public function makecena($cenaFields = [])
    {
        /** @var cenaRepository $cenaRepo */
        $cenaRepo = App::make(cenaRepository::class);
        $theme = $this->fakecenaData($cenaFields);
        return $cenaRepo->create($theme);
    }

    /**
     * Get fake instance of cena
     *
     * @param array $cenaFields
     * @return cena
     */
    public function fakecena($cenaFields = [])
    {
        return new cena($this->fakecenaData($cenaFields));
    }

    /**
     * Get fake data of cena
     *
     * @param array $postFields
     * @return array
     */
    public function fakecenaData($cenaFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'naslov' => $fake->word,
            'tekst' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $cenaFields);
    }
}
