<?php

use Faker\Factory as Faker;
use App\Models\vest;
use App\Repositories\vestRepository;

trait MakevestTrait
{
    /**
     * Create fake instance of vest and save it in database
     *
     * @param array $vestFields
     * @return vest
     */
    public function makevest($vestFields = [])
    {
        /** @var vestRepository $vestRepo */
        $vestRepo = App::make(vestRepository::class);
        $theme = $this->fakevestData($vestFields);
        return $vestRepo->create($theme);
    }

    /**
     * Get fake instance of vest
     *
     * @param array $vestFields
     * @return vest
     */
    public function fakevest($vestFields = [])
    {
        return new vest($this->fakevestData($vestFields));
    }

    /**
     * Get fake data of vest
     *
     * @param array $postFields
     * @return array
     */
    public function fakevestData($vestFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'naslov' => $fake->text,
            'tekst' => $fake->text,
            'slug' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $vestFields);
    }
}
