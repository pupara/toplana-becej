<?php

use Faker\Factory as Faker;
use App\Models\brojilo;
use App\Repositories\brojiloRepository;

trait MakebrojiloTrait
{
    /**
     * Create fake instance of brojilo and save it in database
     *
     * @param array $brojiloFields
     * @return brojilo
     */
    public function makebrojilo($brojiloFields = [])
    {
        /** @var brojiloRepository $brojiloRepo */
        $brojiloRepo = App::make(brojiloRepository::class);
        $theme = $this->fakebrojiloData($brojiloFields);
        return $brojiloRepo->create($theme);
    }

    /**
     * Get fake instance of brojilo
     *
     * @param array $brojiloFields
     * @return brojilo
     */
    public function fakebrojilo($brojiloFields = [])
    {
        return new brojilo($this->fakebrojiloData($brojiloFields));
    }

    /**
     * Get fake data of brojilo
     *
     * @param array $postFields
     * @return array
     */
    public function fakebrojiloData($brojiloFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'naslov' => $fake->text,
            'tekst' => $fake->text,
            'slika' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $brojiloFields);
    }
}
