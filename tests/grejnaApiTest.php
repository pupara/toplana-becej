<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class grejnaApiTest extends TestCase
{
    use MakegrejnaTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreategrejna()
    {
        $grejna = $this->fakegrejnaData();
        $this->json('POST', '/api/v1/grejnas', $grejna);

        $this->assertApiResponse($grejna);
    }

    /**
     * @test
     */
    public function testReadgrejna()
    {
        $grejna = $this->makegrejna();
        $this->json('GET', '/api/v1/grejnas/'.$grejna->id);

        $this->assertApiResponse($grejna->toArray());
    }

    /**
     * @test
     */
    public function testUpdategrejna()
    {
        $grejna = $this->makegrejna();
        $editedgrejna = $this->fakegrejnaData();

        $this->json('PUT', '/api/v1/grejnas/'.$grejna->id, $editedgrejna);

        $this->assertApiResponse($editedgrejna);
    }

    /**
     * @test
     */
    public function testDeletegrejna()
    {
        $grejna = $this->makegrejna();
        $this->json('DELETE', '/api/v1/grejnas/'.$grejna->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/grejnas/'.$grejna->id);

        $this->assertResponseStatus(404);
    }
}
