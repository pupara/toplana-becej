<?php

use App\Models\brojilo;
use App\Repositories\brojiloRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class brojiloRepositoryTest extends TestCase
{
    use MakebrojiloTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var brojiloRepository
     */
    protected $brojiloRepo;

    public function setUp()
    {
        parent::setUp();
        $this->brojiloRepo = App::make(brojiloRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatebrojilo()
    {
        $brojilo = $this->fakebrojiloData();
        $createdbrojilo = $this->brojiloRepo->create($brojilo);
        $createdbrojilo = $createdbrojilo->toArray();
        $this->assertArrayHasKey('id', $createdbrojilo);
        $this->assertNotNull($createdbrojilo['id'], 'Created brojilo must have id specified');
        $this->assertNotNull(brojilo::find($createdbrojilo['id']), 'brojilo with given id must be in DB');
        $this->assertModelData($brojilo, $createdbrojilo);
    }

    /**
     * @test read
     */
    public function testReadbrojilo()
    {
        $brojilo = $this->makebrojilo();
        $dbbrojilo = $this->brojiloRepo->find($brojilo->id);
        $dbbrojilo = $dbbrojilo->toArray();
        $this->assertModelData($brojilo->toArray(), $dbbrojilo);
    }

    /**
     * @test update
     */
    public function testUpdatebrojilo()
    {
        $brojilo = $this->makebrojilo();
        $fakebrojilo = $this->fakebrojiloData();
        $updatedbrojilo = $this->brojiloRepo->update($fakebrojilo, $brojilo->id);
        $this->assertModelData($fakebrojilo, $updatedbrojilo->toArray());
        $dbbrojilo = $this->brojiloRepo->find($brojilo->id);
        $this->assertModelData($fakebrojilo, $dbbrojilo->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletebrojilo()
    {
        $brojilo = $this->makebrojilo();
        $resp = $this->brojiloRepo->delete($brojilo->id);
        $this->assertTrue($resp);
        $this->assertNull(brojilo::find($brojilo->id), 'brojilo should not exist in DB');
    }
}
