<?php

use App\Models\cena;
use App\Repositories\cenaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class cenaRepositoryTest extends TestCase
{
    use MakecenaTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var cenaRepository
     */
    protected $cenaRepo;

    public function setUp()
    {
        parent::setUp();
        $this->cenaRepo = App::make(cenaRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatecena()
    {
        $cena = $this->fakecenaData();
        $createdcena = $this->cenaRepo->create($cena);
        $createdcena = $createdcena->toArray();
        $this->assertArrayHasKey('id', $createdcena);
        $this->assertNotNull($createdcena['id'], 'Created cena must have id specified');
        $this->assertNotNull(cena::find($createdcena['id']), 'cena with given id must be in DB');
        $this->assertModelData($cena, $createdcena);
    }

    /**
     * @test read
     */
    public function testReadcena()
    {
        $cena = $this->makecena();
        $dbcena = $this->cenaRepo->find($cena->id);
        $dbcena = $dbcena->toArray();
        $this->assertModelData($cena->toArray(), $dbcena);
    }

    /**
     * @test update
     */
    public function testUpdatecena()
    {
        $cena = $this->makecena();
        $fakecena = $this->fakecenaData();
        $updatedcena = $this->cenaRepo->update($fakecena, $cena->id);
        $this->assertModelData($fakecena, $updatedcena->toArray());
        $dbcena = $this->cenaRepo->find($cena->id);
        $this->assertModelData($fakecena, $dbcena->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletecena()
    {
        $cena = $this->makecena();
        $resp = $this->cenaRepo->delete($cena->id);
        $this->assertTrue($resp);
        $this->assertNull(cena::find($cena->id), 'cena should not exist in DB');
    }
}
