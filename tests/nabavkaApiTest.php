<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class nabavkaApiTest extends TestCase
{
    use MakenabavkaTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatenabavka()
    {
        $nabavka = $this->fakenabavkaData();
        $this->json('POST', '/api/v1/nabavkas', $nabavka);

        $this->assertApiResponse($nabavka);
    }

    /**
     * @test
     */
    public function testReadnabavka()
    {
        $nabavka = $this->makenabavka();
        $this->json('GET', '/api/v1/nabavkas/'.$nabavka->id);

        $this->assertApiResponse($nabavka->toArray());
    }

    /**
     * @test
     */
    public function testUpdatenabavka()
    {
        $nabavka = $this->makenabavka();
        $editednabavka = $this->fakenabavkaData();

        $this->json('PUT', '/api/v1/nabavkas/'.$nabavka->id, $editednabavka);

        $this->assertApiResponse($editednabavka);
    }

    /**
     * @test
     */
    public function testDeletenabavka()
    {
        $nabavka = $this->makenabavka();
        $this->json('DELETE', '/api/v1/nabavkas/'.$nabavka->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/nabavkas/'.$nabavka->id);

        $this->assertResponseStatus(404);
    }
}
