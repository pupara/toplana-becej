<?php

use App\Models\vest;
use App\Repositories\vestRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class vestRepositoryTest extends TestCase
{
    use MakevestTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var vestRepository
     */
    protected $vestRepo;

    public function setUp()
    {
        parent::setUp();
        $this->vestRepo = App::make(vestRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatevest()
    {
        $vest = $this->fakevestData();
        $createdvest = $this->vestRepo->create($vest);
        $createdvest = $createdvest->toArray();
        $this->assertArrayHasKey('id', $createdvest);
        $this->assertNotNull($createdvest['id'], 'Created vest must have id specified');
        $this->assertNotNull(vest::find($createdvest['id']), 'vest with given id must be in DB');
        $this->assertModelData($vest, $createdvest);
    }

    /**
     * @test read
     */
    public function testReadvest()
    {
        $vest = $this->makevest();
        $dbvest = $this->vestRepo->find($vest->id);
        $dbvest = $dbvest->toArray();
        $this->assertModelData($vest->toArray(), $dbvest);
    }

    /**
     * @test update
     */
    public function testUpdatevest()
    {
        $vest = $this->makevest();
        $fakevest = $this->fakevestData();
        $updatedvest = $this->vestRepo->update($fakevest, $vest->id);
        $this->assertModelData($fakevest, $updatedvest->toArray());
        $dbvest = $this->vestRepo->find($vest->id);
        $this->assertModelData($fakevest, $dbvest->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletevest()
    {
        $vest = $this->makevest();
        $resp = $this->vestRepo->delete($vest->id);
        $this->assertTrue($resp);
        $this->assertNull(vest::find($vest->id), 'vest should not exist in DB');
    }
}
