<?php

use App\Models\nabavka;
use App\Repositories\nabavkaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class nabavkaRepositoryTest extends TestCase
{
    use MakenabavkaTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var nabavkaRepository
     */
    protected $nabavkaRepo;

    public function setUp()
    {
        parent::setUp();
        $this->nabavkaRepo = App::make(nabavkaRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatenabavka()
    {
        $nabavka = $this->fakenabavkaData();
        $creatednabavka = $this->nabavkaRepo->create($nabavka);
        $creatednabavka = $creatednabavka->toArray();
        $this->assertArrayHasKey('id', $creatednabavka);
        $this->assertNotNull($creatednabavka['id'], 'Created nabavka must have id specified');
        $this->assertNotNull(nabavka::find($creatednabavka['id']), 'nabavka with given id must be in DB');
        $this->assertModelData($nabavka, $creatednabavka);
    }

    /**
     * @test read
     */
    public function testReadnabavka()
    {
        $nabavka = $this->makenabavka();
        $dbnabavka = $this->nabavkaRepo->find($nabavka->id);
        $dbnabavka = $dbnabavka->toArray();
        $this->assertModelData($nabavka->toArray(), $dbnabavka);
    }

    /**
     * @test update
     */
    public function testUpdatenabavka()
    {
        $nabavka = $this->makenabavka();
        $fakenabavka = $this->fakenabavkaData();
        $updatednabavka = $this->nabavkaRepo->update($fakenabavka, $nabavka->id);
        $this->assertModelData($fakenabavka, $updatednabavka->toArray());
        $dbnabavka = $this->nabavkaRepo->find($nabavka->id);
        $this->assertModelData($fakenabavka, $dbnabavka->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletenabavka()
    {
        $nabavka = $this->makenabavka();
        $resp = $this->nabavkaRepo->delete($nabavka->id);
        $this->assertTrue($resp);
        $this->assertNull(nabavka::find($nabavka->id), 'nabavka should not exist in DB');
    }
}
