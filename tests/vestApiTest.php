<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class vestApiTest extends TestCase
{
    use MakevestTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatevest()
    {
        $vest = $this->fakevestData();
        $this->json('POST', '/api/v1/vests', $vest);

        $this->assertApiResponse($vest);
    }

    /**
     * @test
     */
    public function testReadvest()
    {
        $vest = $this->makevest();
        $this->json('GET', '/api/v1/vests/'.$vest->id);

        $this->assertApiResponse($vest->toArray());
    }

    /**
     * @test
     */
    public function testUpdatevest()
    {
        $vest = $this->makevest();
        $editedvest = $this->fakevestData();

        $this->json('PUT', '/api/v1/vests/'.$vest->id, $editedvest);

        $this->assertApiResponse($editedvest);
    }

    /**
     * @test
     */
    public function testDeletevest()
    {
        $vest = $this->makevest();
        $this->json('DELETE', '/api/v1/vests/'.$vest->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/vests/'.$vest->id);

        $this->assertResponseStatus(404);
    }
}
