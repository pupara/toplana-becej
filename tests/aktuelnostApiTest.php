<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class aktuelnostApiTest extends TestCase
{
    use MakeaktuelnostTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateaktuelnost()
    {
        $aktuelnost = $this->fakeaktuelnostData();
        $this->json('POST', '/api/v1/aktuelnosts', $aktuelnost);

        $this->assertApiResponse($aktuelnost);
    }

    /**
     * @test
     */
    public function testReadaktuelnost()
    {
        $aktuelnost = $this->makeaktuelnost();
        $this->json('GET', '/api/v1/aktuelnosts/'.$aktuelnost->id);

        $this->assertApiResponse($aktuelnost->toArray());
    }

    /**
     * @test
     */
    public function testUpdateaktuelnost()
    {
        $aktuelnost = $this->makeaktuelnost();
        $editedaktuelnost = $this->fakeaktuelnostData();

        $this->json('PUT', '/api/v1/aktuelnosts/'.$aktuelnost->id, $editedaktuelnost);

        $this->assertApiResponse($editedaktuelnost);
    }

    /**
     * @test
     */
    public function testDeleteaktuelnost()
    {
        $aktuelnost = $this->makeaktuelnost();
        $this->json('DELETE', '/api/v1/aktuelnosts/'.$aktuelnost->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/aktuelnosts/'.$aktuelnost->id);

        $this->assertResponseStatus(404);
    }
}
