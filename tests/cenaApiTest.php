<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class cenaApiTest extends TestCase
{
    use MakecenaTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatecena()
    {
        $cena = $this->fakecenaData();
        $this->json('POST', '/api/v1/cenas', $cena);

        $this->assertApiResponse($cena);
    }

    /**
     * @test
     */
    public function testReadcena()
    {
        $cena = $this->makecena();
        $this->json('GET', '/api/v1/cenas/'.$cena->id);

        $this->assertApiResponse($cena->toArray());
    }

    /**
     * @test
     */
    public function testUpdatecena()
    {
        $cena = $this->makecena();
        $editedcena = $this->fakecenaData();

        $this->json('PUT', '/api/v1/cenas/'.$cena->id, $editedcena);

        $this->assertApiResponse($editedcena);
    }

    /**
     * @test
     */
    public function testDeletecena()
    {
        $cena = $this->makecena();
        $this->json('DELETE', '/api/v1/cenas/'.$cena->iidd);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/cenas/'.$cena->id);

        $this->assertResponseStatus(404);
    }
}
